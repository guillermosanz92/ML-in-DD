Autor: Guillermo Sanz
# Abstract Machine Learning in Drug Discovery

##### English

In this repository we have generated several artificial intelligence models to search for molecules that help treat tuberculosis.
In this way, having the .sdf data of a molecule, we can see the probability that it has to be effective (with a calculated precision of 82.76%) when it comes to inhibiting the enzyme serine protease Rv3671c 

##### Español

En este repositorio hemos generado varios modelos de inteligencia artificial para la busqueda de moleculas que ayuden a tratar la tuberculosis.
De esta forma teniendo los datos .sdf de una molecula podemos ver la probabilidad que tiene de ser efectiva (con una precisión calculada del 82.76%) a la hora de inhibir la enzima serin proteasa Rv3671c 

[Python Code](Notebook.ipynb)  <- Here we can see the analysis performed  

[Exploratory Data Analysis](AutoML_1/EDA/README.md)  

| Best model   | name                                                         | model_type     | metric_type   |   metric_value |   train_time |
|:-------------|:-------------------------------------------------------------|:---------------|:--------------|---------------:|-------------:|
|              | [1_Baseline](AutoML_1/1_Baseline/README.md)                           | Baseline       | logloss       |       0.693237 |         4.19 |
|              | [2_DecisionTree](AutoML_1/2_DecisionTree/README.md)                   | Decision Tree  | logloss       |       1.50391  |        14.71 |
|              | [3_Default_Xgboost](AutoML_1/3_Default_Xgboost/README.md)             | Xgboost        | logloss       |       0.492566 |         8.56 |
|              | [4_Default_NeuralNetwork](AutoML_1/4_Default_NeuralNetwork/README.md) | Neural Network | logloss       |       1.10412  |         7.63 |
|              | [5_Default_RandomForest](AutoML_1/5_Default_RandomForest/README.md)   | Random Forest  | logloss       |       0.425718 |        11.11 |
| **the best** | [Ensemble](AutoML_1/Ensemble/README.md)                               | Ensemble       | logloss       |       0.41047  |         0.27 |

### AutoML Performance
![AutoML Performance](Plots/ldb_performance.png)

### AutoML Performance Boxplot
![AutoML Performance Boxplot](Plots/ldb_performance_boxplot.png)

### Features Importance
![features importance across models](Plots/features_heatmap.png)

hmax: Maximum H E-State  
maxHBint6:Maximum E-State descriptors of strength for potential Hydrogen Bonds of path length 6  
minHBint2:Minimum E-State descriptors of strength for potential Hydrogen Bonds of path length 2  





### Spearman Correlation of Models
![models spearman correlation](Plots/correlation_heatmap.png)

