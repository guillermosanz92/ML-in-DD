# Exploratory Data Analysis


[<< Go back](../README.md)
## Feature : target
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4690265486725664
- **Std** :0.5012626282730047
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](target.png)
## Feature : nAcid
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.34219674071394435
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nAcid.png)
## Feature : ALogP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-0.6270318584070796
- **Std** :1.2517237706077768
- **Min** :-6.3452
- **25%th Percentile** : -1.1395
- **50%th Percentile** : -0.4856
- **75%th Percentile** : 0.1124
- **Max** :1.889

![](ALogP.png)
## Feature : ALogp2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :1.946115752920355
- **Std** :5.041142689116694
- **Min** :0.00023716
- **25%th Percentile** : 0.09400356
- **50%th Percentile** : 0.605439609999999
- **75%th Percentile** : 1.60731684
- **Max** :40.26156304

![](ALogp2.png)
## Feature : AMR
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :39.94329823008849
- **Std** :19.960787322889836
- **Min** :3.3
- **25%th Percentile** : 24.4712
- **50%th Percentile** : 37.4185
- **75%th Percentile** : 50.5542
- **Max** :107.6642

![](AMR.png)
## Feature : apol
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :49.742302539822994
- **Std** :13.382264943831231
- **Min** :25.141137
- **25%th Percentile** : 39.965516
- **50%th Percentile** : 48.941895
- **75%th Percentile** : 55.646239
- **Max** :115.298134

![](apol.png)
## Feature : naAromAtom
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :13.283185840707965
- **Std** :5.00975659971312
- **Min** :0.0
- **25%th Percentile** : 11.0
- **50%th Percentile** : 12.0
- **75%th Percentile** : 17.0
- **Max** :27.0

![](naAromAtom.png)
## Feature : nAromBond
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :13.91150442477876
- **Std** :5.664043107502005
- **Min** :0.0
- **25%th Percentile** : 11.0
- **50%th Percentile** : 12.0
- **75%th Percentile** : 18.0
- **Max** :30.0

![](nAromBond.png)
## Feature : nAtom
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 42
- **Count** :113.0
- **Mean** :40.982300884955755
- **Std** :11.811754361111284
- **Min** :21.0
- **25%th Percentile** : 33.0
- **50%th Percentile** : 39.0
- **75%th Percentile** : 48.0
- **Max** :100.0

![](nAtom.png)
## Feature : nHeavyAtom
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 27
- **Count** :113.0
- **Mean** :24.530973451327434
- **Std** :6.602313983505244
- **Min** :11.0
- **25%th Percentile** : 20.0
- **50%th Percentile** : 24.0
- **75%th Percentile** : 28.0
- **Max** :62.0

![](nHeavyAtom.png)
## Feature : nH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 27
- **Count** :113.0
- **Mean** :16.451327433628318
- **Std** :6.014849407986748
- **Min** :6.0
- **25%th Percentile** : 12.0
- **50%th Percentile** : 16.0
- **75%th Percentile** : 20.0
- **Max** :39.0

![](nH.png)
## Feature : nB
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nB.png)
## Feature : nC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 22
- **Count** :113.0
- **Mean** :18.008849557522122
- **Std** :4.966846468659609
- **Min** :9.0
- **25%th Percentile** : 14.0
- **50%th Percentile** : 18.0
- **75%th Percentile** : 20.0
- **Max** :42.0

![](nC.png)
## Feature : nN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :2.6991150442477876
- **Std** :1.4933480825230336
- **Min** :0.0
- **25%th Percentile** : 2.0
- **50%th Percentile** : 3.0
- **75%th Percentile** : 4.0
- **Max** :6.0

![](nN.png)
## Feature : nO
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :3.1769911504424777
- **Std** :2.5079519550520817
- **Min** :0.0
- **25%th Percentile** : 2.0
- **50%th Percentile** : 3.0
- **75%th Percentile** : 4.0
- **Max** :20.0

![](nO.png)
## Feature : nS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.34513274336283184
- **Std** :0.578944722016224
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nS.png)
## Feature : nP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nP.png)
## Feature : nF
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.5064376963938392
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.0

![](nF.png)
## Feature : nCl
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3472394292688233
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nCl.png)
## Feature : nBr
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2902220258234583
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nBr.png)
## Feature : nI
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nI.png)
## Feature : nX
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.2920353982300885
- **Std** :0.6771976912405212
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.0

![](nX.png)
## Feature : ATSc1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.0113531727842857
- **Std** :15.704209058834955
- **Min** :0.0822884129802039
- **25%th Percentile** : 0.363361950484813
- **50%th Percentile** : 0.478016088286941
- **75%th Percentile** : 0.700213513679005
- **Max** :167.445191805831

![](ATSc1.png)
## Feature : ATSc2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-1.1843641238796565
- **Std** :9.990631260236757
- **Min** :-106.435891449645
- **25%th Percentile** : -0.332064245938035
- **50%th Percentile** : -0.213750164739894
- **75%th Percentile** : -0.155365575052161
- **Max** :0.0346387218782879

![](ATSc2.png)
## Feature : ATSc3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.20540929543630118
- **Std** :2.1574158497395164
- **Min** :-0.527926326362059
- **25%th Percentile** : -0.0476778879418439
- **50%th Percentile** : -0.0066139608298448
- **75%th Percentile** : 0.0769977462960699
- **Max** :22.8965753850162

![](ATSc3.png)
## Feature : ATSc4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-0.023413598097009016
- **Std** :0.1663379615448297
- **Min** :-0.653226657187175
- **25%th Percentile** : -0.10006598953818
- **50%th Percentile** : -0.016598173507751
- **75%th Percentile** : 0.0659794394557779
- **Max** :0.452852633981907

![](ATSc4.png)
## Feature : ATSc5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-0.021508398261361802
- **Std** :0.1540513111236139
- **Min** :-0.506259817245437
- **25%th Percentile** : -0.107583136108937
- **50%th Percentile** : -0.0333152788756449
- **75%th Percentile** : 0.0529811866049391
- **Max** :0.631207760014641

![](ATSc5.png)
## Feature : ATSm1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :34.56094435529266
- **Std** :15.213785089371106
- **Min** :12.1344493368174
- **25%th Percentile** : 25.6288789749113
- **50%th Percentile** : 30.4578564434403
- **75%th Percentile** : 38.9818485814836
- **Max** :116.108487362888

![](ATSm1.png)
## Feature : ATSm2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :31.44320985911356
- **Std** :9.157163500527373
- **Min** :11.6644558434935
- **25%th Percentile** : 25.9987271863087
- **50%th Percentile** : 30.1723572566937
- **75%th Percentile** : 35.6777197824565
- **Max** :76.9702074511815

![](ATSm2.png)
## Feature : ATSm3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :45.59229506557112
- **Std** :14.056501632007281
- **Min** :15.1627297538065
- **25%th Percentile** : 38.0192735450669
- **50%th Percentile** : 42.539215603648
- **75%th Percentile** : 52.5720954693116
- **Max** :121.717371273201

![](ATSm3.png)
## Feature : ATSm4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :48.547483548511
- **Std** :17.926554477848722
- **Min** :14.1627297538065
- **25%th Percentile** : 37.8803257338987
- **50%th Percentile** : 46.0112415649525
- **75%th Percentile** : 55.7145361181071
- **Max** :157.234043328854

![](ATSm4.png)
## Feature : ATSm5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :46.02314485310513
- **Std** :20.2952152795174
- **Min** :10.0517355073796
- **25%th Percentile** : 33.0148235906256
- **50%th Percentile** : 43.4200326467945
- **75%th Percentile** : 52.5488994473705
- **Max** :164.562411237385

![](ATSm5.png)
## Feature : ATSp1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :1705.94861557162
- **Std** :584.3283664338892
- **Min** :572.706434567871
- **25%th Percentile** : 1303.56646968802
- **50%th Percentile** : 1606.73155015954
- **75%th Percentile** : 2059.73412465763
- **Max** :4871.43431357543

![](ATSp1.png)
## Feature : ATSp2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2020.6401169965118
- **Std** :732.7852692442581
- **Min** :621.764666010254
- **25%th Percentile** : 1513.88630408539
- **50%th Percentile** : 1893.23724923571
- **75%th Percentile** : 2431.48827790243
- **Max** :6145.05290515029

![](ATSp2.png)
## Feature : ATSp3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2873.0717955100554
- **Std** :1155.3581033593382
- **Min** :776.723174937256
- **25%th Percentile** : 2098.76443421612
- **50%th Percentile** : 2684.31994145956
- **75%th Percentile** : 3500.41408221305
- **Max** :9593.74068856434

![](ATSp3.png)
## Feature : ATSp4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2922.2052850253895
- **Std** :1410.2035381551398
- **Min** :681.270515380372
- **25%th Percentile** : 2047.91776493371
- **50%th Percentile** : 2625.11280686542
- **75%th Percentile** : 3524.60939593199
- **Max** :11677.9681775389

![](ATSp4.png)
## Feature : ATSp5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2566.6932302575415
- **Std** :1478.9049234984514
- **Min** :410.063893927735
- **25%th Percentile** : 1585.9309031716
- **50%th Percentile** : 2228.44299076666
- **75%th Percentile** : 3213.9927680458
- **Max** :12223.7873517199

![](ATSp5.png)
## Feature : nBase
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.5198341659131008
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :4.0

![](nBase.png)
## Feature : BCUTw.1l
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 53
- **Count** :113.0
- **Mean** :11.925144920458658
- **Std** :0.06486669224878187
- **Min** :11.8
- **25%th Percentile** : 11.85
- **50%th Percentile** : 11.9
- **75%th Percentile** : 11.9961414237921
- **Max** :11.999

![](BCUTw.1l.png)
## Feature : BCUTw.1h
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :26.409056256694416
- **Std** :16.723075830240813
- **Min** :14.004087167828
- **25%th Percentile** : 15.996925220875
- **50%th Percentile** : 15.9999555741429
- **75%th Percentile** : 31.9720727965578
- **Max** :79.9165215223378

![](BCUTw.1h.png)
## Feature : BCUTc.1l
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-0.38702991300384404
- **Std** :0.45690280716592435
- **Min** :-5.17804814068949
- **25%th Percentile** : -0.383644884306163
- **50%th Percentile** : -0.35400173137726
- **75%th Percentile** : -0.306135910497332
- **Max** :-0.264898928606197

![](BCUTc.1l.png)
## Feature : BCUTc.1h
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.3353287793660247
- **Std** :0.9964919612578484
- **Min** :0.0507856403337085
- **25%th Percentile** : 0.217545858966171
- **50%th Percentile** : 0.244573521438208
- **75%th Percentile** : 0.277886185356244
- **Max** :10.8141127219856

![](BCUTc.1h.png)
## Feature : BCUTp.1l
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :4.853271881310724
- **Std** :0.7217961500224
- **Min** :3.6239892635372
- **25%th Percentile** : 4.40291875689358
- **50%th Percentile** : 4.84728966345678
- **75%th Percentile** : 5.21227040241848
- **Max** :6.90678515625001

![](BCUTp.1l.png)
## Feature : BCUTp.1h
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :11.007854323982924
- **Std** :1.1604911515985936
- **Min** :8.48998951985022
- **25%th Percentile** : 10.2097122425238
- **50%th Percentile** : 10.9411644445194
- **75%th Percentile** : 11.8944634978635
- **Max** :14.4424133003901

![](BCUTp.1h.png)
## Feature : nBonds
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 27
- **Count** :113.0
- **Mean** :26.831858407079647
- **Std** :7.353209521391779
- **Min** :11.0
- **25%th Percentile** : 22.0
- **50%th Percentile** : 26.0
- **75%th Percentile** : 31.0
- **Max** :69.0

![](nBonds.png)
## Feature : nBonds2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 44
- **Count** :113.0
- **Mean** :43.283185840707965
- **Std** :12.440909637841624
- **Min** :22.0
- **25%th Percentile** : 35.0
- **50%th Percentile** : 42.0
- **75%th Percentile** : 50.0
- **Max** :107.0

![](nBonds2.png)
## Feature : nBondsS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 42
- **Count** :113.0
- **Mean** :40.86725663716814
- **Std** :11.941452729865855
- **Min** :21.0
- **25%th Percentile** : 32.0
- **50%th Percentile** : 39.0
- **75%th Percentile** : 47.0
- **Max** :103.0

![](nBondsS.png)
## Feature : nBondsS2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :26.95575221238938
- **Std** :11.218121007461683
- **Min** :10.0
- **25%th Percentile** : 18.0
- **50%th Percentile** : 25.0
- **75%th Percentile** : 33.0
- **Max** :79.0

![](nBondsS2.png)
## Feature : nBondsS3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 25
- **Count** :113.0
- **Mean** :10.504424778761061
- **Std** :6.013561895839671
- **Min** :0.0
- **25%th Percentile** : 6.0
- **50%th Percentile** : 10.0
- **75%th Percentile** : 14.0
- **Max** :41.0

![](nBondsS3.png)
## Feature : nBondsD
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :2.309734513274336
- **Std** :1.4147163144305852
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 3.0
- **Max** :6.0

![](nBondsD.png)
## Feature : nBondsD2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :2.309734513274336
- **Std** :1.4147163144305852
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 3.0
- **Max** :6.0

![](nBondsD2.png)
## Feature : nBondsT
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.36260052444625346
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nBondsT.png)
## Feature : nBondsQ
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nBondsQ.png)
## Feature : nBondsM
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 25
- **Count** :113.0
- **Mean** :16.327433628318584
- **Std** :5.380922779790965
- **Min** :2.0
- **25%th Percentile** : 14.0
- **50%th Percentile** : 15.0
- **75%th Percentile** : 20.0
- **Max** :32.0

![](nBondsM.png)
## Feature : bpol
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :24.658653212389375
- **Std** :8.694272078519132
- **Min** :10.498863
- **25%th Percentile** : 18.154484
- **50%th Percentile** : 23.862105
- **75%th Percentile** : 29.52814
- **Max** :53.037866

![](bpol.png)
## Feature : C1SP1
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.36260052444625346
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](C1SP1.png)
## Feature : C2SP1
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](C2SP1.png)
## Feature : C1SP2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 6
- **Count** :113.0
- **Mean** :1.8761061946902655
- **Std** :1.2329750849269296
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 3.0
- **Max** :5.0

![](C1SP2.png)
## Feature : C2SP2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 17
- **Count** :113.0
- **Mean** :10.017699115044248
- **Std** :3.6400115308023655
- **Min** :1.0
- **25%th Percentile** : 8.0
- **50%th Percentile** : 10.0
- **75%th Percentile** : 12.0
- **Max** :23.0

![](C2SP2.png)
## Feature : C3SP2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :2.336283185840708
- **Std** :1.677639814638765
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 3.0
- **Max** :10.0

![](C3SP2.png)
## Feature : C1SP3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :1.6017699115044248
- **Std** :1.6286487771903868
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :7.0

![](C1SP3.png)
## Feature : C2SP3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :1.0442477876106195
- **Std** :2.067561454134927
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :14.0

![](C2SP3.png)
## Feature : C3SP3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.2743362831858407
- **Std** :0.7469914372551333
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :4.0

![](C3SP3.png)
## Feature : C4SP3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](C4SP3.png)
## Feature : SCH.3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SCH.3.png)
## Feature : SCH.4
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SCH.4.png)
## Feature : SCH.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 17
- **Count** :113.0
- **Mean** :0.06905554992281866
- **Std** :0.08053591502813098
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0641500299099584
- **75%th Percentile** : 0.117851130197758
- **Max** :0.331927305260453

![](SCH.5.png)
## Feature : SCH.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 87
- **Count** :113.0
- **Mean** :0.31536403851351275
- **Std** :0.15506255133171232
- **Min** :0.0680413817439772
- **25%th Percentile** : 0.196735636239962
- **50%th Percentile** : 0.268728739282632
- **75%th Percentile** : 0.423946127396402
- **Max** :0.78465135316433

![](SCH.6.png)
## Feature : SCH.7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 106
- **Count** :113.0
- **Mean** :0.5437618225238772
- **Std** :0.2359612553239557
- **Min** :0.117851130197758
- **25%th Percentile** : 0.398295903482419
- **50%th Percentile** : 0.498289684191567
- **75%th Percentile** : 0.664737438438376
- **Max** :1.2645173668352

![](SCH.7.png)
## Feature : VCH.3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](VCH.3.png)
## Feature : VCH.4
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](VCH.4.png)
## Feature : VCH.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 38
- **Count** :113.0
- **Mean** :0.030953802572180254
- **Std** :0.04222831574494404
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0204124145231932
- **75%th Percentile** : 0.0559016994374947
- **Max** :0.173874575027092

![](VCH.5.png)
## Feature : VCH.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 105
- **Count** :113.0
- **Mean** :0.11906335463033088
- **Std** :0.07951351533572744
- **Min** :0.0240562612162344
- **25%th Percentile** : 0.0695309804149292
- **50%th Percentile** : 0.0943927129298043
- **75%th Percentile** : 0.152062072615966
- **Max** :0.58524059554805

![](VCH.6.png)
## Feature : VCH.7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 109
- **Count** :113.0
- **Mean** :0.1702496390686223
- **Std** :0.12045927634255077
- **Min** :0.0387340886388865
- **25%th Percentile** : 0.100914177847619
- **50%th Percentile** : 0.143314273406962
- **75%th Percentile** : 0.189304179407495
- **Max** :0.83282464420072

![](VCH.7.png)
## Feature : SC.3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 105
- **Count** :113.0
- **Mean** :1.7361588062914703
- **Std** :0.7556773779096677
- **Min** :0.408248290463863
- **25%th Percentile** : 1.21199193260336
- **50%th Percentile** : 1.59083030736292
- **75%th Percentile** : 2.16629258838897
- **Max** :5.22200295924852

![](SC.3.png)
## Feature : SC.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.038324009689666066
- **Std** :0.09557463554828192
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.492799279826744

![](SC.4.png)
## Feature : SC.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 89
- **Count** :113.0
- **Mean** :0.47434109180442835
- **Std** :0.30420873482609107
- **Min** :0.0
- **25%th Percentile** : 0.293217603751632
- **50%th Percentile** : 0.412272071894346
- **75%th Percentile** : 0.62151245673974
- **Max** :2.0874484345904

![](SC.5.png)
## Feature : SC.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.01970832292216737
- **Std** :0.05103566697066685
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.246399639913372

![](SC.6.png)
## Feature : VC.3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :0.7595653873520337
- **Std** :0.3366009634275735
- **Min** :0.262891711531604
- **25%th Percentile** : 0.539985768546976
- **50%th Percentile** : 0.673776636156584
- **75%th Percentile** : 0.880925711499341
- **Max** :2.17692538760681

![](VC.3.png)
## Feature : VC.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :0.010149859028259526
- **Std** :0.035148583388028946
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.25

![](VC.4.png)
## Feature : VC.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 102
- **Count** :113.0
- **Mean** :0.13151680554655404
- **Std** :0.09214024563549418
- **Min** :0.0
- **25%th Percentile** : 0.0647992282199108
- **50%th Percentile** : 0.121008742549967
- **75%th Percentile** : 0.169333355099182
- **Max** :0.510857476496277

![](VC.5.png)
## Feature : VC.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.0029493978243152075
- **Std** :0.010679398290768544
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0833333333333333

![](VC.6.png)
## Feature : SPC.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 107
- **Count** :113.0
- **Mean** :4.068730754406919
- **Std** :1.7171954273252326
- **Min** :0.866025403784439
- **25%th Percentile** : 2.91845557140479
- **50%th Percentile** : 3.77431143205861
- **75%th Percentile** : 4.86964371091952
- **Max** :14.3918907295658

![](SPC.4.png)
## Feature : SPC.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :6.564811426883554
- **Std** :2.95942364729729
- **Min** :1.1921054089425
- **25%th Percentile** : 4.62793579447172
- **50%th Percentile** : 5.99461736421287
- **75%th Percentile** : 7.83763587391766
- **Max** :24.0530399112911

![](SPC.5.png)
## Feature : SPC.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :9.354818193924096
- **Std** :4.931769192492258
- **Min** :1.3634395377601
- **25%th Percentile** : 6.19127967817357
- **50%th Percentile** : 8.73210794612188
- **75%th Percentile** : 11.5605726995912
- **Max** :38.2454011591633

![](SPC.6.png)
## Feature : VPC.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :1.5286504562447858
- **Std** :0.6975070134560858
- **Min** :0.455530800603653
- **25%th Percentile** : 1.11429299733246
- **50%th Percentile** : 1.40559895004208
- **75%th Percentile** : 1.76111191395286
- **Max** :4.89703831487077

![](VPC.4.png)
## Feature : VPC.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.1053575718605835
- **Std** :1.1170850068970277
- **Min** :0.53311617112367
- **25%th Percentile** : 1.37356529775739
- **50%th Percentile** : 1.90856062443816
- **75%th Percentile** : 2.50450166374347
- **Max** :7.14101020187638

![](VPC.5.png)
## Feature : VPC.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.4996412743655667
- **Std** :1.5626171256292658
- **Min** :0.43641062384892
- **25%th Percentile** : 1.5015504680139
- **50%th Percentile** : 2.21270829629924
- **75%th Percentile** : 3.04253703634899
- **Max** :9.80963678356184

![](VPC.6.png)
## Feature : SP.0
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 99
- **Count** :113.0
- **Mean** :17.419856230368445
- **Std** :4.764508102829507
- **Min** :8.26758471350162
- **25%th Percentile** : 14.1125196961929
- **50%th Percentile** : 16.5351694270032
- **75%th Percentile** : 19.9574546788841
- **Max** :44.6342165746735

![](SP.0.png)
## Feature : SP.1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 106
- **Count** :113.0
- **Mean** :11.820559189488478
- **Std** :3.142571652860416
- **Min** :5.23638210521845
- **25%th Percentile** : 9.73718344301788
- **50%th Percentile** : 11.669035095596
- **75%th Percentile** : 13.4222539532201
- **Max** :29.4785797212328

![](SP.1.png)
## Feature : SP.2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :10.74189569355992
- **Std** :3.053522106303174
- **Min** :4.44426291928105
- **25%th Percentile** : 8.67415421060372
- **50%th Percentile** : 10.2296529226862
- **75%th Percentile** : 12.4459377008275
- **Max** :28.0796149752361

![](SP.2.png)
## Feature : SP.3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :9.033340225224242
- **Std** :2.6943685822170673
- **Min** :3.28412990558947
- **25%th Percentile** : 7.25225645270467
- **50%th Percentile** : 8.7376460956121
- **75%th Percentile** : 10.4258358611709
- **Max** :25.6025002603275

![](SP.3.png)
## Feature : SP.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :7.581840887274799
- **Std** :2.4144348599378564
- **Min** :2.42325605152029
- **25%th Percentile** : 6.13226555000279
- **50%th Percentile** : 7.28122416783223
- **75%th Percentile** : 8.82989975482156
- **Max** :21.9160783198472

![](SP.4.png)
## Feature : SP.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :6.068106928730137
- **Std** :2.1326567201296207
- **Min** :1.95578542441497
- **25%th Percentile** : 4.81354759003041
- **50%th Percentile** : 5.89285413749221
- **75%th Percentile** : 7.22012833193432
- **Max** :18.679738666342

![](SP.5.png)
## Feature : SP.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :4.204182032927985
- **Std** :1.7733601622628452
- **Min** :0.827987837980596
- **25%th Percentile** : 3.18388028472491
- **50%th Percentile** : 3.99496927025807
- **75%th Percentile** : 5.09366784293287
- **Max** :15.063050334473

![](SP.6.png)
## Feature : SP.7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :3.0057915916501683
- **Std** :1.4851275570445719
- **Min** :0.271304514140263
- **25%th Percentile** : 2.14650084124838
- **50%th Percentile** : 2.87383217768073
- **75%th Percentile** : 3.7894214058467
- **Max** :12.2373868564838

![](SP.7.png)
## Feature : VP.0
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :13.802679304123282
- **Std** :3.6309759227572473
- **Min** :6.78117853694805
- **25%th Percentile** : 11.2231323684662
- **50%th Percentile** : 13.2226122729867
- **75%th Percentile** : 15.7952725904606
- **Max** :31.7484689542553

![](VP.0.png)
## Feature : VP.1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :8.056574023606858
- **Std** :2.2194955125051044
- **Min** :3.75725593514069
- **25%th Percentile** : 6.6131233175413
- **50%th Percentile** : 7.68470285828952
- **75%th Percentile** : 9.01926267067559
- **Max** :18.9836665630593

![](VP.1.png)
## Feature : VP.2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :6.0736716374252175
- **Std** :1.829299880608802
- **Min** :2.52955437942151
- **25%th Percentile** : 4.76216226152
- **50%th Percentile** : 5.70157202065838
- **75%th Percentile** : 6.86911496727606
- **Max** :15.214101109944

![](VP.2.png)
## Feature : VP.3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :4.345255963498855
- **Std** :1.4464370285280093
- **Min** :1.58838140035616
- **25%th Percentile** : 3.33796253665933
- **50%th Percentile** : 4.2256182177506
- **75%th Percentile** : 4.97789614753654
- **Max** :11.671863291756

![](VP.3.png)
## Feature : VP.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :3.0827226648729082
- **Std** :1.1766922719041903
- **Min** :0.990891380203131
- **25%th Percentile** : 2.32226030961063
- **50%th Percentile** : 2.96920704270332
- **75%th Percentile** : 3.60421978443804
- **Max** :8.70974018574656

![](VP.4.png)
## Feature : VP.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.0885873756401887
- **Std** :0.9189753533684948
- **Min** :0.553831009395198
- **25%th Percentile** : 1.47388403877651
- **50%th Percentile** : 1.94614146791454
- **75%th Percentile** : 2.46952046512335
- **Max** :6.53466151521889

![](VP.5.png)
## Feature : VP.6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :1.2070564020473498
- **Std** :0.6521871639652913
- **Min** :0.216491382739524
- **25%th Percentile** : 0.795467418341611
- **50%th Percentile** : 1.0892052928333
- **75%th Percentile** : 1.50698781080523
- **Max** :4.41133289814236

![](VP.6.png)
## Feature : VP.7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.7269674028701126
- **Std** :0.4573346470665988
- **Min** :0.0673682912135097
- **25%th Percentile** : 0.430848930362156
- **50%th Percentile** : 0.625712134845277
- **75%th Percentile** : 0.950132948098414
- **Max** :3.00238810682651

![](VP.7.png)
## Feature : CrippenLogP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :3.496163628318585
- **Std** :1.5773256657178487
- **Min** :-0.42925
- **25%th Percentile** : 2.41797
- **50%th Percentile** : 3.62215
- **75%th Percentile** : 4.28617
- **Max** :9.31514999999999

![](CrippenLogP.png)
## Feature : CrippenMR
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :96.54203628318585
- **Std** :23.503455766360425
- **Min** :48.8387
- **25%th Percentile** : 78.9706
- **50%th Percentile** : 97.0712
- **75%th Percentile** : 109.8074
- **Max** :208.0053

![](CrippenMR.png)
## Feature : ECCEN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 106
- **Count** :113.0
- **Mean** :536.4867256637168
- **Std** :284.01581680818185
- **Min** :101.0
- **25%th Percentile** : 339.0
- **50%th Percentile** : 481.0
- **75%th Percentile** : 644.0
- **Max** :1944.0

![](ECCEN.png)
## Feature : nHBd
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :1.7079646017699115
- **Std** :1.5793387852945855
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 2.0
- **Max** :12.0

![](nHBd.png)
## Feature : nwHBd
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nwHBd.png)
## Feature : nHBa
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :5.017699115044247
- **Std** :2.566898840753162
- **Min** :1.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 5.0
- **75%th Percentile** : 6.0
- **Max** :20.0

![](nHBa.png)
## Feature : nwHBa
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 21
- **Count** :113.0
- **Mean** :14.654867256637168
- **Std** :4.34817266936112
- **Min** :3.0
- **25%th Percentile** : 12.0
- **50%th Percentile** : 14.0
- **75%th Percentile** : 17.0
- **Max** :29.0

![](nwHBa.png)
## Feature : nHBint2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 6
- **Count** :113.0
- **Mean** :1.0265486725663717
- **Std** :1.2353439153238608
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :5.0

![](nHBint2.png)
## Feature : nHBint3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.7876106194690266
- **Std** :1.8099493256206907
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :16.0

![](nHBint3.png)
## Feature : nHBint4
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :1.2035398230088497
- **Std** :2.188277584390609
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 2.0
- **Max** :14.0

![](nHBint4.png)
## Feature : nHBint5
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :1.3274336283185841
- **Std** :2.2655907376763245
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :14.0

![](nHBint5.png)
## Feature : nHBint6
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :0.8407079646017699
- **Std** :1.5327750958020543
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :8.0

![](nHBint6.png)
## Feature : nHBint7
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.8053097345132744
- **Std** :1.394238188553967
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :8.0

![](nHBint7.png)
## Feature : nHBint8
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 6
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :1.1520058909106288
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.0

![](nHBint8.png)
## Feature : nHBint9
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :0.6460176991150443
- **Std** :1.9082544098052243
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :16.0

![](nHBint9.png)
## Feature : nHBint10
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :1.4907002062555927
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :14.0

![](nHBint10.png)
## Feature : nHsOH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.5663716814159292
- **Std** :1.387933369867191
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :12.0

![](nHsOH.png)
## Feature : nHdNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nHdNH.png)
## Feature : nHsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nHsSH.png)
## Feature : nHsNH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.18584070796460178
- **Std** :0.45412145493035877
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nHsNH2.png)
## Feature : nHssNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.7967419434167472
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nHssNH.png)
## Feature : nHaaNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.13274336283185842
- **Std** :0.4331039296825278
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nHaaNH.png)
## Feature : nHsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nHsNH3p.png)
## Feature : nHssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nHssNH2p.png)
## Feature : nHsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nHsssNHp.png)
## Feature : nHtCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nHtCH.png)
## Feature : nHdCH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720274
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nHdCH2.png)
## Feature : nHdsCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.4424778761061947
- **Std** :0.743280050775724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :4.0

![](nHdsCH.png)
## Feature : nHaaCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 15
- **Count** :113.0
- **Mean** :6.946902654867257
- **Std** :2.8717861165776073
- **Min** :0.0
- **25%th Percentile** : 5.0
- **50%th Percentile** : 7.0
- **75%th Percentile** : 9.0
- **Max** :16.0

![](nHaaCH.png)
## Feature : nHCHnX
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nHCHnX.png)
## Feature : nHCsats
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :2.150442477876106
- **Std** :3.1231315526763175
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.0
- **Max** :16.0

![](nHCsats.png)
## Feature : nHCsatu
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.5929203539823009
- **Std** :0.89280025103196
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :4.0

![](nHCsatu.png)
## Feature : nHAvin
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.18584070796460178
- **Std** :0.39071072068300994
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nHAvin.png)
## Feature : nHother
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :7.415929203539823
- **Std** :2.902356381696032
- **Min** :0.0
- **25%th Percentile** : 5.0
- **50%th Percentile** : 8.0
- **75%th Percentile** : 9.0
- **Max** :19.0

![](nHother.png)
## Feature : nHmisc
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nHmisc.png)
## Feature : nsLi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsLi.png)
## Feature : nssBe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssBe.png)
## Feature : nssssBem
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssBem.png)
## Feature : nsBH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsBH2.png)
## Feature : nssBH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssBH.png)
## Feature : nsssB
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssB.png)
## Feature : nssssBm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssBm.png)
## Feature : nsCH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 6
- **Count** :113.0
- **Mean** :1.1061946902654867
- **Std** :1.096771494777864
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :5.0

![](nsCH3.png)
## Feature : ndCH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720274
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](ndCH2.png)
## Feature : nssCH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :1.6283185840707965
- **Std** :2.3421399336987854
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :15.0

![](nssCH2.png)
## Feature : ntCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](ntCH.png)
## Feature : ndsCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.4424778761061947
- **Std** :0.743280050775724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :4.0

![](ndsCH.png)
## Feature : naaCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 15
- **Count** :113.0
- **Mean** :6.946902654867257
- **Std** :2.8717861165776073
- **Min** :0.0
- **25%th Percentile** : 5.0
- **50%th Percentile** : 7.0
- **75%th Percentile** : 9.0
- **Max** :16.0

![](naaCH.png)
## Feature : nsssCH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.5221238938053098
- **Std** :1.4583340858465341
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :12.0

![](nsssCH.png)
## Feature : nddC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nddC.png)
## Feature : ntsC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.36260052444625346
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](ntsC.png)
## Feature : ndssC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :1.9026548672566372
- **Std** :1.6199900890895658
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 3.0
- **Max** :8.0

![](ndssC.png)
## Feature : naasC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :4.371681415929204
- **Std** :2.0796406380219317
- **Min** :0.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 4.0
- **75%th Percentile** : 5.0
- **Max** :14.0

![](naasC.png)
## Feature : naaaC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.8495575221238938
- **Std** :1.3901518348345279
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 2.0
- **Max** :4.0

![](naaaC.png)
## Feature : nssssC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.3030085135636213
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nssssC.png)
## Feature : nsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsNH3p.png)
## Feature : nsNH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.18584070796460178
- **Std** :0.45412145493035877
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nsNH2.png)
## Feature : nssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssNH2p.png)
## Feature : ndNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](ndNH.png)
## Feature : nssNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.7967419434167472
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nssNH.png)
## Feature : naaNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.13274336283185842
- **Std** :0.4331039296825278
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](naaNH.png)
## Feature : ntN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.3526583172823516
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](ntN.png)
## Feature : nsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssNHp.png)
## Feature : ndsN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.2831858407079646
- **Std** :0.4718698849991941
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](ndsN.png)
## Feature : naaN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.5486725663716814
- **Std** :0.7558244132175651
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](naaN.png)
## Feature : nsssN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.34513274336283184
- **Std** :0.623496928398105
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nsssN.png)
## Feature : nddsN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nddsN.png)
## Feature : naasN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562113
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](naasN.png)
## Feature : nssssNp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssNp.png)
## Feature : nsOH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.5663716814159292
- **Std** :1.387933369867191
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :12.0

![](nsOH.png)
## Feature : ndO
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :1.5575221238938053
- **Std** :1.1255003101584988
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :4.0

![](ndO.png)
## Feature : nssO
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 6
- **Count** :113.0
- **Mean** :0.7522123893805309
- **Std** :1.073470452146145
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :5.0

![](nssO.png)
## Feature : naaO
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.4059840007310108
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](naaO.png)
## Feature : naOm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](naOm.png)
## Feature : nsOm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3568899230763265
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nsOm.png)
## Feature : nsF
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.5064376963938392
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.0

![](nsF.png)
## Feature : nsSiH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsSiH3.png)
## Feature : nssSiH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssSiH2.png)
## Feature : nsssSiH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssSiH.png)
## Feature : nssssSi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssSi.png)
## Feature : nsPH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsPH2.png)
## Feature : nssPH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssPH.png)
## Feature : nsssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssP.png)
## Feature : ndsssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](ndsssP.png)
## Feature : nddsP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nddsP.png)
## Feature : nsssssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssssP.png)
## Feature : nsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsSH.png)
## Feature : ndS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333468995
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](ndS.png)
## Feature : nssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nssS.png)
## Feature : naaS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.31502613621428904
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](naaS.png)
## Feature : ndssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](ndssS.png)
## Feature : nddssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562096
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nddssS.png)
## Feature : nssssssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssssS.png)
## Feature : nSm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nSm.png)
## Feature : nsCl
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3472394292688233
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nsCl.png)
## Feature : nsGeH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsGeH3.png)
## Feature : nssGeH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssGeH2.png)
## Feature : nsssGeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssGeH.png)
## Feature : nssssGe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssGe.png)
## Feature : nsAsH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsAsH2.png)
## Feature : nssAsH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssAsH.png)
## Feature : nsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssAs.png)
## Feature : ndsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](ndsssAs.png)
## Feature : nddsAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nddsAs.png)
## Feature : nsssssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssssAs.png)
## Feature : nsSeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsSeH.png)
## Feature : ndSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](ndSe.png)
## Feature : nssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nssSe.png)
## Feature : naaSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](naaSe.png)
## Feature : ndssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](ndssSe.png)
## Feature : nssssssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssssSe.png)
## Feature : nddssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nddssSe.png)
## Feature : nsBr
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2902220258234583
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nsBr.png)
## Feature : nsSnH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsSnH3.png)
## Feature : nssSnH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssSnH2.png)
## Feature : nsssSnH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssSnH.png)
## Feature : nssssSn
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssSn.png)
## Feature : nsI
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsI.png)
## Feature : nsPbH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsPbH3.png)
## Feature : nssPbH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssPbH2.png)
## Feature : nsssPbH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nsssPbH.png)
## Feature : nssssPb
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nssssPb.png)
## Feature : SHBd
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 91
- **Count** :113.0
- **Mean** :0.8133298687579109
- **Std** :0.7112656410947419
- **Min** :0.0
- **25%th Percentile** : 0.302209112811791
- **50%th Percentile** : 0.752293146210025
- **75%th Percentile** : 1.17695880223072
- **Max** :4.29244791620098

![](SHBd.png)
## Feature : SwHBd
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.004674570937300956
- **Std** :0.04969137067548085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.528226515915008

![](SwHBd.png)
## Feature : SHBa
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :35.381148467767794
- **Std** :25.017319390801518
- **Min** :3.15326798311917
- **25%th Percentile** : 20.7537018893235
- **50%th Percentile** : 31.3597597951445
- **75%th Percentile** : 44.9667110719796
- **Max** :203.39450806474

![](SHBa.png)
## Feature : SwHBa
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :16.442274101412025
- **Std** :8.374828721546287
- **Min** :1.1466166176513
- **25%th Percentile** : 11.0817792411017
- **50%th Percentile** : 16.0251379440665
- **75%th Percentile** : 21.9887638523766
- **Max** :49.9306504345812

![](SwHBa.png)
## Feature : SHBint2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 61
- **Count** :113.0
- **Mean** :4.295809526822723
- **Std** :5.398946271551429
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.673414881793357
- **75%th Percentile** : 7.43713674036978
- **Max** :19.1003036513732

![](SHBint2.png)
## Feature : SHBint3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :2.242949325224335
- **Std** :6.050239406875326
- **Min** :-1.01030754003763
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 2.1751106628476
- **Max** :42.9121993249163

![](SHBint3.png)
## Feature : SHBint4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 50
- **Count** :113.0
- **Mean** :4.290850184960722
- **Std** :8.309265281710152
- **Min** :-0.52662100412104
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 5.31900125953918
- **Max** :44.7341095602958

![](SHBint4.png)
## Feature : SHBint5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 59
- **Count** :113.0
- **Mean** :4.349361480467281
- **Std** :7.844316161558341
- **Min** :-0.605943056673049
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 5.87683145057159
- **Max** :47.5460344788997

![](SHBint5.png)
## Feature : SHBint6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :3.2154451106575506
- **Std** :6.327313434790924
- **Min** :-0.59155699936534
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.33400585121591
- **Max** :42.3250200176931

![](SHBint6.png)
## Feature : SHBint7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 43
- **Count** :113.0
- **Mean** :2.838380423278514
- **Std** :5.2618667940490615
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.11418541651415
- **Max** :22.708503836034

![](SHBint7.png)
## Feature : SHBint8
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 18
- **Count** :113.0
- **Mean** :1.258593341318234
- **Std** :4.333802461671063
- **Min** :-1.02615310815017
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :25.0008806028604

![](SHBint8.png)
## Feature : SHBint9
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 26
- **Count** :113.0
- **Mean** :2.463845197708412
- **Std** :7.04682717481524
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :56.9208885310512

![](SHBint9.png)
## Feature : SHBint10
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :1.3822211052656237
- **Std** :6.258305122710595
- **Min** :-0.383855260543686
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :53.4901734782866

![](SHBint10.png)
## Feature : SHsOH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :0.2708020447359345
- **Std** :0.5486012152416219
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.535828985314968
- **Max** :4.29244791620098

![](SHsOH.png)
## Feature : SHdNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SHdNH.png)
## Feature : SHsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SHsSH.png)
## Feature : SHsNH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :0.0991562922753505
- **Std** :0.24110612458652686
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.11490433673469

![](SHsNH2.png)
## Feature : SHssNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :0.37863042873180935
- **Std** :0.40711317917401074
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.34734800170068
- **75%th Percentile** : 0.571236890589569
- **Max** :1.67629906515075

![](SHssNH.png)
## Feature : SHaaNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.059618189016121016
- **Std** :0.1924113028101269
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.906254161578161

![](SHaaNH.png)
## Feature : SHsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SHsNH3p.png)
## Feature : SHssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SHssNH2p.png)
## Feature : SHsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SHsssNHp.png)
## Feature : SHtCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.005122913998695302
- **Std** :0.05445732279223058
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.578889281852569

![](SHtCH.png)
## Feature : SHdCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.01238797513554084
- **Std** :0.07581532379584777
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.534592191043084

![](SHdCH2.png)
## Feature : SHdsCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :0.2771856768600692
- **Std** :0.44892121765495024
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.650449263038549
- **Max** :2.43373299319728

![](SHdsCH.png)
## Feature : SHaaCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :3.7935937137226206
- **Std** :1.4961239162367825
- **Min** :0.0
- **25%th Percentile** : 2.94127428980852
- **50%th Percentile** : 3.87797626143507
- **75%th Percentile** : 4.65314819890307
- **Max** :7.73960339539882

![](SHaaCH.png)
## Feature : SHCHnX
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.004674570937300956
- **Std** :0.04969137067548085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.528226515915008

![](SHCHnX.png)
## Feature : SHCsats
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 52
- **Count** :113.0
- **Mean** :1.220646908088988
- **Std** :1.9878446932093596
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 2.00819777014632
- **Max** :14.872809018548

![](SHCsats.png)
## Feature : SHCsatu
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 42
- **Count** :113.0
- **Mean** :0.4392804523037692
- **Std** :0.6763823441136927
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.707910772914155
- **Max** :2.60295209750567

![](SHCsatu.png)
## Feature : SHAvin
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 22
- **Count** :113.0
- **Mean** :0.1235710400128667
- **Std** :0.2610796417806936
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.766041666666667

![](SHAvin.png)
## Feature : SHother
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :4.083167365718231
- **Std** :1.5355574580517364
- **Min** :0.0
- **25%th Percentile** : 3.13384227244617
- **50%th Percentile** : 4.14168591742253
- **75%th Percentile** : 4.96293359402459
- **Max** :9.40759375821061

![](SHother.png)
## Feature : SHmisc
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SHmisc.png)
## Feature : SsLi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsLi.png)
## Feature : SssBe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssBe.png)
## Feature : SssssBem
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssBem.png)
## Feature : SsBH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsBH2.png)
## Feature : SssBH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssBH.png)
## Feature : SsssB
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssB.png)
## Feature : SssssBm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssBm.png)
## Feature : SsCH3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 73
- **Count** :113.0
- **Mean** :1.9931989243045503
- **Std** :1.9907739121429968
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.8090009236528
- **75%th Percentile** : 3.4971793547028
- **Max** :7.86001019581046

![](SsCH3.png)
## Feature : SdCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.10174966900925239
- **Std** :0.6189497591738392
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.91434875493407

![](SdCH2.png)
## Feature : SssCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 64
- **Count** :113.0
- **Mean** :1.039092557883017
- **Std** :2.5991780430043177
- **Min** :-1.64216893702573
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.11584624590577
- **Max** :20.6937342289746

![](SssCH2.png)
## Feature : StCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.048182455703457965
- **Std** :0.5121865297433866
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :5.44461749449075

![](StCH.png)
## Feature : SdsCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :0.6964717984537808
- **Std** :1.2544823623634833
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.36299107142857
- **Max** :6.09253306878307

![](SdsCH.png)
## Feature : SaaCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :12.491756653860266
- **Std** :5.823819019119694
- **Min** :0.0
- **25%th Percentile** : 8.06110730927538
- **50%th Percentile** : 12.7637703809777
- **75%th Percentile** : 16.5265585684457
- **Max** :33.6080728054341

![](SaaCH.png)
## Feature : SsssCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 30
- **Count** :113.0
- **Mean** :-0.30963289957842965
- **Std** :2.0660365472796736
- **Min** :-20.5186323945633
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.18573976049542

![](SsssCH.png)
## Feature : SddC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SddC.png)
## Feature : StsC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.21860970703953034
- **Std** :0.7465830593857689
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :4.13843616150164

![](StsC.png)
## Feature : SdssC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 91
- **Count** :113.0
- **Mean** :-0.2597499453799259
- **Std** :1.207901028542375
- **Min** :-6.83903075106178
- **25%th Percentile** : -0.4882350718065
- **50%th Percentile** : -0.103055555555556
- **75%th Percentile** : 0.0129718337952465
- **Max** :2.55382532333921

![](SdssC.png)
## Feature : SaasC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :2.355021714790368
- **Std** :1.9594410056800593
- **Min** :-5.20679323091674
- **25%th Percentile** : 1.04416570434758
- **50%th Percentile** : 2.4676381470139
- **75%th Percentile** : 3.62028128225323
- **Max** :7.52836714557221

![](SaasC.png)
## Feature : SaaaC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :0.7902320479352948
- **Std** :1.3596401833956246
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.55544419774083
- **Max** :4.62465750188965

![](SaaaC.png)
## Feature : SssssC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :-0.17186645249200225
- **Std** :0.8222143060729439
- **Min** :-4.85323000477357
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0286944910287628

![](SssssC.png)
## Feature : SsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsNH3p.png)
## Feature : SsNH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :1.0656268477729967
- **Std** :2.6247041381230387
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :12.0762186109012

![](SsNH2.png)
## Feature : SssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssNH2p.png)
## Feature : SdNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SdNH.png)
## Feature : SssNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :2.309286862618572
- **Std** :2.2266248093233956
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 2.69341049382716
- **75%th Percentile** : 3.28683177437642
- **Max** :8.91120351882114

![](SssNH.png)
## Feature : SaaNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.4258536209574046
- **Std** :1.3990985706177694
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.62241276964311

![](SaaNH.png)
## Feature : StN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.8911156745831693
- **Std** :3.206322685851511
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :17.8461797682036

![](StN.png)
## Feature : SsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssNHp.png)
## Feature : SdsN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 32
- **Count** :113.0
- **Mean** :1.1683071591805114
- **Std** :1.9627730868401012
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 3.84088477366255
- **Max** :8.72857640055097

![](SdsN.png)
## Feature : SaaN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 47
- **Count** :113.0
- **Mean** :2.3393739273498277
- **Std** :3.2074708452231815
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.42872186875975
- **Max** :13.1903260952812

![](SaaN.png)
## Feature : SsssN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 32
- **Count** :113.0
- **Mean** :0.5843858850761249
- **Std** :1.1260279555763284
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.26740740740741
- **Max** :6.08489452286345

![](SsssN.png)
## Feature : SddsN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SddsN.png)
## Feature : SaasN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.1757946062986524
- **Std** :0.5467677897829695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.24481481481481

![](SaasN.png)
## Feature : SssssNp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssNp.png)
## Feature : SsOH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :5.6634066420888045
- **Std** :14.48684042609606
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 9.22397231653094
- **Max** :126.126017861485

![](SsOH.png)
## Feature : SdO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 93
- **Count** :113.0
- **Mean** :18.977884386531933
- **Std** :13.863708936869719
- **Min** :0.0
- **25%th Percentile** : 11.6764076968406
- **50%th Percentile** : 13.1108650323842
- **75%th Percentile** : 25.6381879272089
- **Max** :54.2396005857634

![](SdO.png)
## Feature : SssO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 50
- **Count** :113.0
- **Mean** :4.036551115027269
- **Std** :5.7885382480491625
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 5.46663973749381
- **Max** :26.4527733599306

![](SssO.png)
## Feature : SaaO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 20
- **Count** :113.0
- **Mean** :0.9427746617573273
- **Std** :2.174174029897134
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :10.8717094671636

![](SaaO.png)
## Feature : SaOm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SaOm.png)
## Feature : SsOm
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :1.3710449869708283
- **Std** :3.951381982945804
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :22.1425546711207

![](SsOm.png)
## Feature : SsF
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :1.3949978109902557
- **Std** :6.642038995171921
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :40.0984307629715

![](SsF.png)
## Feature : SsSiH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsSiH3.png)
## Feature : SssSiH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssSiH2.png)
## Feature : SsssSiH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssSiH.png)
## Feature : SssssSi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssSi.png)
## Feature : SsPH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsPH2.png)
## Feature : SssPH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssPH.png)
## Feature : SsssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssP.png)
## Feature : SdsssP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SdsssP.png)
## Feature : SddsP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SddsP.png)
## Feature : SsssssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssssP.png)
## Feature : SsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsSH.png)
## Feature : SdS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.03359351551783107
- **Std** :0.13291304296840525
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.828069873697078

![](SdS.png)
## Feature : SssS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :-0.13053115286035186
- **Std** :0.4587195131938263
- **Min** :-2.13424592740809
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssS.png)
## Feature : SaaS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :-0.12331128839943355
- **Std** :0.45802171220200605
- **Min** :-3.2066900770454
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SaaS.png)
## Feature : SdssS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SdssS.png)
## Feature : SddssS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :-0.5321400567896478
- **Std** :1.6326440363386687
- **Min** :-6.15348402083742
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SddssS.png)
## Feature : SssssssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssssS.png)
## Feature : SSm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SSm.png)
## Feature : SsCl
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 13
- **Count** :113.0
- **Mean** :0.05842846990823237
- **Std** :0.19071995183039298
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.908789504656458

![](SsCl.png)
## Feature : SsGeH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsGeH3.png)
## Feature : SssGeH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssGeH2.png)
## Feature : SsssGeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssGeH.png)
## Feature : SssssGe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssGe.png)
## Feature : SsAsH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsAsH2.png)
## Feature : SssAsH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssAsH.png)
## Feature : SsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssAs.png)
## Feature : SdsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SdsssAs.png)
## Feature : SddsAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SddsAs.png)
## Feature : SsssssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssssAs.png)
## Feature : SsSeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsSeH.png)
## Feature : SdSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SdSe.png)
## Feature : SssSe
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :-0.014675584507811682
- **Std** :0.1560036032051478
- **Min** :-1.65834104938272
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssSe.png)
## Feature : SaaSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SaaSe.png)
## Feature : SdssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SdssSe.png)
## Feature : SssssssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssssSe.png)
## Feature : SddssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SddssSe.png)
## Feature : SsBr
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :0.009737183729366645
- **Std** :0.06345756643956532
- **Min** :-0.314727065108053
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.319308159492668

![](SsBr.png)
## Feature : SsSnH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsSnH3.png)
## Feature : SssSnH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssSnH2.png)
## Feature : SsssSnH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssSnH.png)
## Feature : SssssSn
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssSn.png)
## Feature : SsI
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsI.png)
## Feature : SsPbH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsPbH3.png)
## Feature : SssPbH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssPbH2.png)
## Feature : SsssPbH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SsssPbH.png)
## Feature : SssssPb
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](SssssPb.png)
## Feature : minHBd
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 91
- **Count** :113.0
- **Mean** :0.34307334431703246
- **Std** :0.2001190344422185
- **Min** :0.0
- **25%th Percentile** : 0.272777777777778
- **50%th Percentile** : 0.386328017832647
- **75%th Percentile** : 0.486987276392038
- **Max** :0.804830678041652

![](minHBd.png)
## Feature : minwHBd
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.004674570937300956
- **Std** :0.04969137067548085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.528226515915008

![](minwHBd.png)
## Feature : minHBa
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.1636224775262867
- **Std** :2.348431223181282
- **Min** :-2.13424592740809
- **25%th Percentile** : 0.80814422136325
- **50%th Percentile** : 2.19916803900164
- **75%th Percentile** : 3.15662037037037
- **Max** :9.74076530612245

![](minHBa.png)
## Feature : minwHBa
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-0.22360353545236009
- **Std** :0.5739857828849956
- **Min** :-1.52736764120027
- **25%th Percentile** : -0.566982001133787
- **50%th Percentile** : -0.313357308201058
- **75%th Percentile** : -0.0399741575030177
- **Max** :1.47980940833123

![](minwHBa.png)
## Feature : minHBint2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 61
- **Count** :113.0
- **Mean** :1.9248901735662887
- **Std** :2.7482036942642227
- **Min** :-0.750877559922417
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.66683380712239
- **Max** :8.12899039782125

![](minHBint2.png)
## Feature : minHBint3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :0.6464862264992915
- **Std** :1.4665844329946573
- **Min** :-1.01030754003763
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.840888077829516
- **Max** :8.20530330919443

![](minHBint3.png)
## Feature : minHBint4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 50
- **Count** :113.0
- **Mean** :0.9742314133100826
- **Std** :1.8359663987188386
- **Min** :-1.10833220283961
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.15376366511097
- **Max** :9.06603281316508

![](minHBint4.png)
## Feature : minHBint5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 59
- **Count** :113.0
- **Mean** :1.020758874423767
- **Std** :1.5714018108021894
- **Min** :-0.825924147703769
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.51283033144291
- **Max** :6.84595421124348

![](minHBint5.png)
## Feature : minHBint6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :0.9792099740807964
- **Std** :1.8679283490326115
- **Min** :-1.05360168546542
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.30667500177195
- **Max** :8.2208184063617

![](minHBint6.png)
## Feature : minHBint7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 43
- **Count** :113.0
- **Mean** :0.9825215283692295
- **Std** :1.8105054713167132
- **Min** :-1.30045281310412
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.44158428843509
- **Max** :8.18919635234776

![](minHBint7.png)
## Feature : minHBint8
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 18
- **Count** :113.0
- **Mean** :0.24924860481957833
- **Std** :0.9112583781492004
- **Min** :-1.02615310815017
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :5.91582965287641

![](minHBint8.png)
## Feature : minHBint9
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 26
- **Count** :113.0
- **Mean** :0.6464991587102014
- **Std** :1.6704766051683815
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :9.49128648640137

![](minHBint9.png)
## Feature : minHBint10
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :0.3408083116843001
- **Std** :1.1587404844287643
- **Min** :-0.383855260543686
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :7.8903536533221

![](minHBint10.png)
## Feature : minHsOH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :0.16100274146118349
- **Std** :0.25313328344611585
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.461042183884029
- **Max** :0.804830678041652

![](minHsOH.png)
## Feature : minHdNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minHdNH.png)
## Feature : minHsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minHsSH.png)
## Feature : minHsNH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :0.08549655706089744
- **Std** :0.1985854081992678
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.662369220836483

![](minHsNH2.png)
## Feature : minHssNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :0.2629218233738275
- **Std** :0.231481623256075
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.327393707482993
- **75%th Percentile** : 0.446515882930676
- **Max** :0.717542831947594

![](minHssNH.png)
## Feature : minHaaNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.043677128613702
- **Std** :0.13456148510177574
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.566032021604938

![](minHaaNH.png)
## Feature : minHsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minHsNH3p.png)
## Feature : minHssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minHssNH2p.png)
## Feature : minHsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minHsssNHp.png)
## Feature : minHtCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.005122913998695302
- **Std** :0.05445732279223058
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.578889281852569

![](minHtCH.png)
## Feature : minHdCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.01238797513554084
- **Std** :0.07581532379584777
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.534592191043084

![](minHdCH2.png)
## Feature : minHdsCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :0.21555079769080437
- **Std** :0.309978863779964
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.613637329931973
- **Max** :0.80843112244898

![](minHdsCH.png)
## Feature : minHaaCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :0.5006100811240817
- **Std** :0.0870626481511277
- **Min** :0.0
- **25%th Percentile** : 0.47332136558327
- **50%th Percentile** : 0.50093112244898
- **75%th Percentile** : 0.545133532292623
- **Max** :0.65344092758392

![](minHaaCH.png)
## Feature : minHCHnX
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.004674570937300956
- **Std** :0.04969137067548085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.528226515915008

![](minHCHnX.png)
## Feature : minHCsats
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 52
- **Count** :113.0
- **Mean** :0.21730083377645792
- **Std** :0.2605990779615771
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.439476892842746
- **Max** :0.955709679705216

![](minHCsats.png)
## Feature : minHCsatu
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :0.26553796029223503
- **Std** :0.3509808202755663
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.585135778848576
- **Max** :1.02901502267574

![](minHCsatu.png)
## Feature : minHAvin
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 22
- **Count** :113.0
- **Mean** :0.1235710400128667
- **Std** :0.2610796417806936
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.766041666666667

![](minHAvin.png)
## Feature : minHother
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.5008894441885544
- **Std** :0.07435068256913392
- **Min** :0.0
- **25%th Percentile** : 0.468110043130701
- **50%th Percentile** : 0.498762020240195
- **75%th Percentile** : 0.543903748837987
- **Max** :0.65344092758392

![](minHother.png)
## Feature : minHmisc
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minHmisc.png)
## Feature : minsLi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsLi.png)
## Feature : minssBe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssBe.png)
## Feature : minssssBem
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssBem.png)
## Feature : minsBH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsBH2.png)
## Feature : minssBH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssBH.png)
## Feature : minsssB
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssB.png)
## Feature : minssssBm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssBm.png)
## Feature : minsCH3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 73
- **Count** :113.0
- **Mean** :1.1167383275115872
- **Std** :0.8697760620191388
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.51341886600319
- **75%th Percentile** : 1.83904809145881
- **Max** :2.28337904768038

![](minsCH3.png)
## Feature : mindCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.10174966900925239
- **Std** :0.6189497591738392
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.91434875493407

![](mindCH2.png)
## Feature : minssCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 64
- **Count** :113.0
- **Mean** :0.1944810798476351
- **Std** :0.3717554600134429
- **Min** :-0.821084468512864
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.488163265306123
- **Max** :1.09666784769463

![](minssCH2.png)
## Feature : mintCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.048182455703457965
- **Std** :0.5121865297433866
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :5.44461749449075

![](mintCH.png)
## Feature : mindsCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :0.5066139378342978
- **Std** :0.7305595657729812
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.33655568443142
- **Max** :2.00392951625094

![](mindsCH.png)
## Feature : minaaCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :1.5643457933079343
- **Std** :0.3585482479024406
- **Min** :0.0
- **25%th Percentile** : 1.40422269098947
- **50%th Percentile** : 1.62871630133535
- **75%th Percentile** : 1.82603363567649
- **Max** :2.05417662920462

![](minaaCH.png)
## Feature : minsssCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 30
- **Count** :113.0
- **Mean** :-0.08992150929901103
- **Std** :0.3557292564379705
- **Min** :-1.94481287674299
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.891528381414294

![](minsssCH.png)
## Feature : minddC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minddC.png)
## Feature : mintsC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.18310094457846593
- **Std** :0.5966065658162586
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.6741459969568

![](mintsC.png)
## Feature : mindssC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 91
- **Count** :113.0
- **Mean** :-0.2966955323277781
- **Std** :0.4386837156948258
- **Min** :-1.52736764120027
- **25%th Percentile** : -0.502750342935528
- **50%th Percentile** : -0.290277706634128
- **75%th Percentile** : 0.0
- **Max** :1.47980940833123

![](mindssC.png)
## Feature : minaasC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :0.2780138466916209
- **Std** :0.45438286881771667
- **Min** :-1.34592697811448
- **25%th Percentile** : -0.0219388325206114
- **50%th Percentile** : 0.311620370370371
- **75%th Percentile** : 0.627110864407491
- **Max** :1.17146810755983

![](minaasC.png)
## Feature : minaaaC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :0.2597713274042008
- **Std** :0.4035177632039697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.7217649281935
- **Max** :1.15103638286712

![](minaaaC.png)
## Feature : minssssC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :-0.17094703175011738
- **Std** :0.8172812436920279
- **Min** :-4.85323000477357
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0286944910287628

![](minssssC.png)
## Feature : minsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsNH3p.png)
## Feature : minsNH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :0.9092957448747565
- **Std** :2.1020281589755885
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.1004095960515

![](minsNH2.png)
## Feature : minssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssNH2p.png)
## Feature : mindNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](mindNH.png)
## Feature : minssNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :1.6954327487714131
- **Std** :1.4186138413135432
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 2.42403470647518
- **75%th Percentile** : 2.95317436696901
- **Max** :3.65815980095742

![](minssNH.png)
## Feature : minaaNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.3094839955073453
- **Std** :0.9479150463591183
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.33038926681784

![](minaaNH.png)
## Feature : mintN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.7336080807145214
- **Std** :2.509194229762565
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :9.88821366819608

![](mintN.png)
## Feature : minsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssNHp.png)
## Feature : mindsN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 32
- **Count** :113.0
- **Mean** :1.126194714990117
- **Std** :1.846735152639976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 3.84088477366255
- **Max** :4.62305240614764

![](mindsN.png)
## Feature : minaaN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 47
- **Count** :113.0
- **Mean** :1.728314600483395
- **Std** :2.108218125050293
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.06988614890401
- **Max** :4.88915375409423

![](minaaN.png)
## Feature : minsssN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 32
- **Count** :113.0
- **Mean** :0.4487798758589471
- **Std** :0.7615120349586514
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.1628514739229
- **Max** :2.55321551923238

![](minsssN.png)
## Feature : minddsN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minddsN.png)
## Feature : minaasN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.1757946062986524
- **Std** :0.5467677897829695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.24481481481481

![](minaasN.png)
## Feature : minssssNp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssNp.png)
## Feature : minsOH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :2.976821780524366
- **Std** :4.473559684292197
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 9.21800721307461
- **Max** :11.0421967513857

![](minsOH.png)
## Feature : mindO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 93
- **Count** :113.0
- **Mean** :9.700403499595106
- **Std** :4.6918747255749835
- **Min** :0.0
- **25%th Percentile** : 10.8614028142887
- **50%th Percentile** : 11.7212244550917
- **75%th Percentile** : 12.2420717494987
- **Max** :13.3018888576509

![](mindO.png)
## Feature : minssO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 50
- **Count** :113.0
- **Mean** :2.283384580427618
- **Std** :2.629370329373131
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 5.19561112221646
- **Max** :6.21785575332279

![](minssO.png)
## Feature : minaaO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 20
- **Count** :113.0
- **Mean** :0.893854478762792
- **Std** :2.0043298773419953
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.21785575332279

![](minaaO.png)
## Feature : minaOm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minaOm.png)
## Feature : minsOm
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :1.2712117846979787
- **Std** :3.5433388663414074
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :11.6092282725848

![](minsOm.png)
## Feature : minsF
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.6996298614251629
- **Std** :2.968424981939787
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :13.5651689810472

![](minsF.png)
## Feature : minsSiH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsSiH3.png)
## Feature : minssSiH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssSiH2.png)
## Feature : minsssSiH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssSiH.png)
## Feature : minssssSi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssSi.png)
## Feature : minsPH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsPH2.png)
## Feature : minssPH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssPH.png)
## Feature : minsssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssP.png)
## Feature : mindsssP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](mindsssP.png)
## Feature : minddsP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minddsP.png)
## Feature : minsssssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssssP.png)
## Feature : minsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsSH.png)
## Feature : mindS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.03359351551783107
- **Std** :0.13291304296840525
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.828069873697078

![](mindS.png)
## Feature : minssS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :-0.13053115286035186
- **Std** :0.4587195131938263
- **Min** :-2.13424592740809
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssS.png)
## Feature : minaaS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :-0.11019862056802603
- **Std** :0.3842248166966654
- **Min** :-1.76431520790758
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minaaS.png)
## Feature : mindssS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](mindssS.png)
## Feature : minddssS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :-0.5321400567896478
- **Std** :1.6326440363386687
- **Min** :-6.15348402083742
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minddssS.png)
## Feature : minssssssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssssS.png)
## Feature : minSm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minSm.png)
## Feature : minsCl
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 13
- **Count** :113.0
- **Mean** :0.05399138676177148
- **Std** :0.17602610169840557
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.80814422136325

![](minsCl.png)
## Feature : minsGeH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsGeH3.png)
## Feature : minssGeH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssGeH2.png)
## Feature : minsssGeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssGeH.png)
## Feature : minssssGe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssGe.png)
## Feature : minsAsH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsAsH2.png)
## Feature : minssAsH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssAsH.png)
## Feature : minsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssAs.png)
## Feature : mindsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](mindsssAs.png)
## Feature : minddsAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minddsAs.png)
## Feature : minsssssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssssAs.png)
## Feature : minsSeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsSeH.png)
## Feature : mindSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](mindSe.png)
## Feature : minssSe
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :-0.014675584507811682
- **Std** :0.1560036032051478
- **Min** :-1.65834104938272
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssSe.png)
## Feature : minaaSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minaaSe.png)
## Feature : mindssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](mindssSe.png)
## Feature : minssssssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssssSe.png)
## Feature : minddssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minddssSe.png)
## Feature : minsBr
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :0.010476432278499115
- **Std** :0.06003843160694693
- **Min** :-0.231191979056084
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.319308159492668

![](minsBr.png)
## Feature : minsSnH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsSnH3.png)
## Feature : minssSnH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssSnH2.png)
## Feature : minsssSnH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssSnH.png)
## Feature : minssssSn
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssSn.png)
## Feature : minsI
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsI.png)
## Feature : minsPbH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsPbH3.png)
## Feature : minssPbH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssPbH2.png)
## Feature : minsssPbH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minsssPbH.png)
## Feature : minssssPb
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](minssssPb.png)
## Feature : maxHBd
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 91
- **Count** :113.0
- **Mean** :0.40709198085769094
- **Std** :0.23269407082003235
- **Min** :0.0
- **25%th Percentile** : 0.302209112811791
- **50%th Percentile** : 0.486987276392038
- **75%th Percentile** : 0.578889281852569
- **Max** :0.804830678041652

![](maxHBd.png)
## Feature : maxwHBd
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.004674570937300956
- **Std** :0.04969137067548085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.528226515915008

![](maxwHBd.png)
## Feature : maxHBa
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :11.127192647283165
- **Std** :2.9111946293105917
- **Min** :3.15326798311917
- **25%th Percentile** : 11.5222582963214
- **50%th Percentile** : 12.1579710726883
- **75%th Percentile** : 12.7769107172695
- **Max** :14.6615636365645

![](maxHBa.png)
## Feature : maxwHBa
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :1.9903966008948564
- **Std** :0.5021377193477324
- **Min** :1.23114414670827
- **25%th Percentile** : 1.81938586545729
- **50%th Percentile** : 1.94228822682782
- **75%th Percentile** : 2.08469752561518
- **Max** :5.44461749449075

![](maxwHBa.png)
## Feature : maxHBint2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 61
- **Count** :113.0
- **Mean** :2.8508773947106723
- **Std** :3.1767005939220656
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.673414881793357
- **75%th Percentile** : 6.27492365820456
- **Max** :8.13442348143217

![](maxHBint2.png)
## Feature : maxHBint3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 37
- **Count** :113.0
- **Mean** :1.1356761892238392
- **Std** :2.022313948558008
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.98119161376207
- **Max** :9.15114244237105

![](maxHBint3.png)
## Feature : maxHBint4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 49
- **Count** :113.0
- **Mean** :1.8356790729259602
- **Std** :2.653980052312056
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 3.96222288963198
- **Max** :9.06603281316508

![](maxHBint4.png)
## Feature : maxHBint5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 58
- **Count** :113.0
- **Mean** :2.110643737087491
- **Std** :2.6212673644702202
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.15905077454452
- **75%th Percentile** : 5.08984599545963
- **Max** :7.24339824473936

![](maxHBint5.png)
## Feature : maxHBint6
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :1.7855717243814024
- **Std** :2.7103215853278564
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.20647458990288
- **Max** :8.594624093671

![](maxHBint6.png)
## Feature : maxHBint7
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 43
- **Count** :113.0
- **Mean** :1.6678275040504478
- **Std** :2.5251967860645217
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 3.04214260215543
- **Max** :8.18919635234776

![](maxHBint7.png)
## Feature : maxHBint8
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 15
- **Count** :113.0
- **Mean** :0.6030870366400448
- **Std** :1.7931710828470415
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :8.02257384806628

![](maxHBint8.png)
## Feature : maxHBint9
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 26
- **Count** :113.0
- **Mean** :1.1575059351317842
- **Std** :2.3769040929045073
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :9.49128648640137

![](maxHBint9.png)
## Feature : maxHBint10
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 13
- **Count** :113.0
- **Mean** :0.4984608659690034
- **Std** :1.6076405912506697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :7.8903536533221

![](maxHBint10.png)
## Feature : maxHsOH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :0.17289012432379017
- **Std** :0.2651279959051564
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.479878218447618
- **Max** :0.804830678041652

![](maxHsOH.png)
## Feature : maxHdNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxHdNH.png)
## Feature : maxHsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxHsSH.png)
## Feature : maxHsNH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :0.08563357999527607
- **Std** :0.19889005010079522
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.662369220836483

![](maxHsNH2.png)
## Feature : maxHssNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :0.28250860677983664
- **Std** :0.24949522356930853
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.34734800170068
- **75%th Percentile** : 0.518592214663643
- **Max** :0.724278943058705

![](maxHssNH.png)
## Feature : maxHaaNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.04497879373719794
- **Std** :0.1382946785437252
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.566032021604938

![](maxHaaNH.png)
## Feature : maxHsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxHsNH3p.png)
## Feature : maxHssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxHssNH2p.png)
## Feature : maxHsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxHsssNHp.png)
## Feature : maxHtCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.005122913998695302
- **Std** :0.05445732279223058
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.578889281852569

![](maxHtCH.png)
## Feature : maxHdCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.01238797513554084
- **Std** :0.07581532379584777
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.534592191043084

![](maxHdCH2.png)
## Feature : maxHdsCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :0.21987158880113142
- **Std** :0.31479567763630384
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.619178240740741
- **Max** :0.80843112244898

![](maxHdsCH.png)
## Feature : maxHaaCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :0.6017820706174207
- **Std** :0.10929619478432402
- **Min** :0.0
- **25%th Percentile** : 0.554369409800958
- **50%th Percentile** : 0.605
- **75%th Percentile** : 0.655818098072562
- **Max** :0.809310661956089

![](maxHaaCH.png)
## Feature : maxHCHnX
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.004674570937300956
- **Std** :0.04969137067548085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.528226515915008

![](maxHCHnX.png)
## Feature : maxHCsats
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 50
- **Count** :113.0
- **Mean** :0.29845778686964247
- **Std** :0.3473433874337707
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.621289890764999
- **Max** :1.25369021798415

![](maxHCsats.png)
## Feature : maxHCsatu
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 42
- **Count** :113.0
- **Mean** :0.293314399587853
- **Std** :0.3879681140077241
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.670592954774502
- **Max** :1.03784722222222

![](maxHCsatu.png)
## Feature : maxHAvin
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 22
- **Count** :113.0
- **Mean** :0.1235710400128667
- **Std** :0.2610796417806936
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.766041666666667

![](maxHAvin.png)
## Feature : maxHother
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :0.6236372037066151
- **Std** :0.09983376191577772
- **Min** :0.0
- **25%th Percentile** : 0.56411932639997
- **50%th Percentile** : 0.625054563492063
- **75%th Percentile** : 0.689652777777778
- **Max** :0.809310661956089

![](maxHother.png)
## Feature : maxHmisc
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxHmisc.png)
## Feature : maxsLi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsLi.png)
## Feature : maxssBe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssBe.png)
## Feature : maxssssBem
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssBem.png)
## Feature : maxsBH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsBH2.png)
## Feature : maxssBH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssBH.png)
## Feature : maxsssB
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssB.png)
## Feature : maxssssBm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssBm.png)
## Feature : maxsCH3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 73
- **Count** :113.0
- **Mean** :1.1768755852014463
- **Std** :0.9118199300048342
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.61001259763165
- **75%th Percentile** : 1.96046217561099
- **Max** :2.28337904768038

![](maxsCH3.png)
## Feature : maxdCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.10174966900925239
- **Std** :0.6189497591738392
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.91434875493407

![](maxdCH2.png)
## Feature : maxssCH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 57
- **Count** :113.0
- **Mean** :0.3540090289344515
- **Std** :0.4146125901411419
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.693342860292265
- **Max** :1.46023562866779

![](maxssCH2.png)
## Feature : maxtCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.048182455703457965
- **Std** :0.5121865297433866
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :5.44461749449075

![](maxtCH.png)
## Feature : maxdsCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 39
- **Count** :113.0
- **Mean** :0.5148304370629079
- **Std** :0.7443579411827108
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.33655568443142
- **Max** :2.07593002536184

![](maxdsCH.png)
## Feature : maxaaCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :1.8609916774128765
- **Std** :0.3317259113029169
- **Min** :0.0
- **25%th Percentile** : 1.7501207010582
- **50%th Percentile** : 1.9080186287478
- **75%th Percentile** : 2.06386419228185
- **Max** :2.30080386641524

![](maxaaCH.png)
## Feature : maxsssCH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :0.031900631407691706
- **Std** :0.12777749738105987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.891528381414294

![](maxsssCH.png)
## Feature : maxddC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxddC.png)
## Feature : maxtsC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.18310094457846593
- **Std** :0.5966065658162586
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.6741459969568

![](maxtsC.png)
## Feature : maxdssC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 47
- **Count** :113.0
- **Mean** :0.24437472322780218
- **Std** :0.37582726694866814
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.507059240362812
- **Max** :1.47980940833123

![](maxdssC.png)
## Feature : maxaasC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :0.8525550147796332
- **Std** :0.3532010093161114
- **Min** :0.0
- **25%th Percentile** : 0.656607824753985
- **50%th Percentile** : 0.855787037037037
- **75%th Percentile** : 1.15708333333333
- **Max** :1.43301867364638

![](maxaasC.png)
## Feature : maxaaaC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :0.30232291936930256
- **Std** :0.4695762135591317
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.778205257411607
- **Max** :1.4776824734021

![](maxaaaC.png)
## Feature : maxssssC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.0002539335489271044
- **Std** :0.002699350651840306
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0286944910287628

![](maxssssC.png)
## Feature : maxsNH3p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsNH3p.png)
## Feature : maxsNH2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 19
- **Count** :113.0
- **Mean** :0.90974764310373
- **Std** :2.1031316363433086
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.1004095960515

![](maxsNH2.png)
## Feature : maxssNH2p
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssNH2p.png)
## Feature : maxdNH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxdNH.png)
## Feature : maxssNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :1.7627509685825216
- **Std** :1.467461703096513
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 2.60777754157218
- **75%th Percentile** : 3.06996359635835
- **Max** :3.65815980095742

![](maxssNH.png)
## Feature : maxaaNH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.3114122875619598
- **Std** :0.9539346691310626
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.35474786018121

![](maxaaNH.png)
## Feature : maxtN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.7336080807145214
- **Std** :2.509194229762565
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :9.88821366819608

![](maxtN.png)
## Feature : maxsssNHp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssNHp.png)
## Feature : maxdsN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 32
- **Count** :113.0
- **Mean** :1.1331755644279764
- **Std** :1.859030526145733
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 3.84088477366255
- **Max** :4.75870619351454

![](maxdsN.png)
## Feature : maxaaN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 47
- **Count** :113.0
- **Mean** :1.762560686659167
- **Std** :2.146912250660553
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 4.1296905374868
- **Max** :4.88915375409423

![](maxaaN.png)
## Feature : maxsssN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 32
- **Count** :113.0
- **Mean** :0.46903365885164644
- **Std** :0.7952112813647197
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.1628514739229
- **Max** :2.73823906105652

![](maxsssN.png)
## Feature : maxddsN
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxddsN.png)
## Feature : maxaasN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :0.1757946062986524
- **Std** :0.5467677897829695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.24481481481481

![](maxaasN.png)
## Feature : maxssssNp
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssNp.png)
## Feature : maxsOH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 36
- **Count** :113.0
- **Mean** :3.0466400093569828
- **Std** :4.585425672141626
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 9.22397231653094
- **Max** :11.4735890652557

![](maxsOH.png)
## Feature : maxdO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 93
- **Count** :113.0
- **Mean** :10.050040293242928
- **Std** :4.860507569191667
- **Min** :0.0
- **25%th Percentile** : 11.4560925296044
- **50%th Percentile** : 12.1425805376482
- **75%th Percentile** : 12.7386205712397
- **Max** :14.6615636365645

![](maxdO.png)
## Feature : maxssO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 50
- **Count** :113.0
- **Mean** :2.3579457885418837
- **Std** :2.717324762901951
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 5.33001907669251
- **Max** :6.21785575332279

![](maxssO.png)
## Feature : maxaaO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 20
- **Count** :113.0
- **Mean** :0.8954850264583791
- **Std** :2.008053714408985
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.21785575332279

![](maxaaO.png)
## Feature : maxaOm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxaOm.png)
## Feature : maxsOm
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :1.274926377994822
- **Std** :3.5536872363936003
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :11.6092282725848

![](maxsOm.png)
## Feature : maxsF
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.6996298614251629
- **Std** :2.968424981939787
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :13.5651689810472

![](maxsF.png)
## Feature : maxsSiH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsSiH3.png)
## Feature : maxssSiH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssSiH2.png)
## Feature : maxsssSiH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssSiH.png)
## Feature : maxssssSi
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssSi.png)
## Feature : maxsPH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsPH2.png)
## Feature : maxssPH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssPH.png)
## Feature : maxsssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssP.png)
## Feature : maxdsssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxdsssP.png)
## Feature : maxddsP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxddsP.png)
## Feature : maxsssssP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssssP.png)
## Feature : maxsSH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsSH.png)
## Feature : maxdS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.03359351551783107
- **Std** :0.13291304296840525
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.828069873697078

![](maxdS.png)
## Feature : maxssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssS.png)
## Feature : maxaaS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxaaS.png)
## Feature : maxdssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxdssS.png)
## Feature : maxddssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxddssS.png)
## Feature : maxssssssS
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssssS.png)
## Feature : maxSm
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxSm.png)
## Feature : maxsCl
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 13
- **Count** :113.0
- **Mean** :0.054823168057733455
- **Std** :0.17792283099305412
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.80814422136325

![](maxsCl.png)
## Feature : maxsGeH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsGeH3.png)
## Feature : maxssGeH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssGeH2.png)
## Feature : maxsssGeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssGeH.png)
## Feature : maxssssGe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssGe.png)
## Feature : maxsAsH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsAsH2.png)
## Feature : maxssAsH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssAsH.png)
## Feature : maxsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssAs.png)
## Feature : maxdsssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxdsssAs.png)
## Feature : maxddsAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxddsAs.png)
## Feature : maxsssssAs
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssssAs.png)
## Feature : maxsSeH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsSeH.png)
## Feature : maxdSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxdSe.png)
## Feature : maxssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssSe.png)
## Feature : maxaaSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxaaSe.png)
## Feature : maxdssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxdssSe.png)
## Feature : maxssssssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssssSe.png)
## Feature : maxddssSe
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxddssSe.png)
## Feature : maxsBr
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :0.012522378995809593
- **Std** :0.05549692835336509
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.319308159492668

![](maxsBr.png)
## Feature : maxsSnH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsSnH3.png)
## Feature : maxssSnH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssSnH2.png)
## Feature : maxsssSnH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssSnH.png)
## Feature : maxssssSn
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssSn.png)
## Feature : maxsI
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsI.png)
## Feature : maxsPbH3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsPbH3.png)
## Feature : maxssPbH2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssPbH2.png)
## Feature : maxsssPbH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxsssPbH.png)
## Feature : maxssssPb
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](maxssssPb.png)
## Feature : sumI
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 109
- **Count** :113.0
- **Mean** :59.739702829673355
- **Std** :19.590489370090225
- **Min** :25.0
- **25%th Percentile** : 47.3333333333333
- **50%th Percentile** : 57.5
- **75%th Percentile** : 68.5833333333333
- **Max** :183.0

![](sumI.png)
## Feature : hmax
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :0.8123714288078578
- **Std** :0.2481335374852242
- **Min** :0.482194822373394
- **25%th Percentile** : 0.648402777777778
- **50%th Percentile** : 0.733025006298816
- **75%th Percentile** : 0.864718114714108
- **Max** :1.60996613756034

![](hmax.png)
## Feature : gmax
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :11.26267330264499
- **Std** :2.6035612137033026
- **Min** :3.76796717754142
- **25%th Percentile** : 11.5222582963214
- **50%th Percentile** : 12.1579710726883
- **75%th Percentile** : 12.7769107172695
- **Max** :14.6615636365645

![](gmax.png)
## Feature : hmin
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.0530029912450555
- **Std** :0.1504683232716378
- **Min** :-0.450365528155707
- **25%th Percentile** : -0.028231292517007
- **50%th Percentile** : 0.0598865425799949
- **75%th Percentile** : 0.16241512345679
- **Max** :0.375225340136054

![](hmin.png)
## Feature : gmin
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :-1.0494527780339418
- **Std** :1.7234656447267545
- **Min** :-6.15348402083742
- **25%th Percentile** : -1.30453000335937
- **50%th Percentile** : -0.511777859419454
- **75%th Percentile** : -0.154048231577092
- **Max** :1.0187037037037

![](gmin.png)
## Feature : LipoaffinityIndex
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :6.0079119009576925
- **Std** :2.750552510920217
- **Min** :-6.28912015256545
- **25%th Percentile** : 4.17858259618234
- **50%th Percentile** : 6.04000630259511
- **75%th Percentile** : 7.54067646579387
- **Max** :16.4129214813101

![](LipoaffinityIndex.png)
## Feature : MAXDN
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.4280558057512818
- **Std** :1.360619063096845
- **Min** :0.58630361866969
- **25%th Percentile** : 1.74518836083816
- **50%th Percentile** : 2.07372325627782
- **75%th Percentile** : 2.5880530098432
- **Max** :6.47755809491149

![](MAXDN.png)
## Feature : MAXDP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :4.7656843723259605
- **Std** :1.42606313273332
- **Min** :0.980579071134627
- **25%th Percentile** : 4.52225829632139
- **50%th Percentile** : 5.14258053764818
- **75%th Percentile** : 5.73862057123967
- **Max** :7.66156363656447

![](MAXDP.png)
## Feature : DELS
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :30.769690779156765
- **Std** :21.25439310766856
- **Min** :5.30119425547997
- **25%th Percentile** : 19.383765301251
- **50%th Percentile** : 26.8360751826311
- **75%th Percentile** : 38.2753798185941
- **Max** :178.78901612948

![](DELS.png)
## Feature : MAXDN2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.3127114429379896
- **Std** :1.1392822201116355
- **Min** :0.647962962962963
- **25%th Percentile** : 1.74518836083816
- **50%th Percentile** : 2.07692397203268
- **75%th Percentile** : 2.53340152278644
- **Max** :6.12693370847727

![](MAXDN2.png)
## Feature : MAXDP2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :4.739722962264169
- **Std** :1.3997675691393228
- **Min** :1.40565531305115
- **25%th Percentile** : 4.39965513983371
- **50%th Percentile** : 5.11822878188363
- **75%th Percentile** : 5.63832816153704
- **Max** :7.66156363656447

![](MAXDP2.png)
## Feature : DELS2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :31.058254560942622
- **Std** :21.212168107666134
- **Min** :5.30119425547997
- **25%th Percentile** : 19.5947814280543
- **50%th Percentile** : 26.8360751826311
- **75%th Percentile** : 38.2753798185941
- **Max** :178.78901612948

![](DELS2.png)
## Feature : ETA_Alpha
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 103
- **Count** :113.0
- **Mean** :11.651343628318587
- **Std** :3.0291550681932455
- **Min** :5.23333
- **25%th Percentile** : 9.46666
- **50%th Percentile** : 11.48095
- **75%th Percentile** : 13.33332
- **Max** :27.6666

![](ETA_Alpha.png)
## Feature : ETA_AlphaP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 105
- **Count** :113.0
- **Mean** :0.4759446017699116
- **Std** :0.017555990584992746
- **Min** :0.43619
- **25%th Percentile** : 0.465
- **50%th Percentile** : 0.47429
- **75%th Percentile** : 0.48229
- **Max** :0.54931

![](ETA_AlphaP.png)
## Feature : ETA_dAlpha_A
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.0015758407079646017
- **Std** :0.0065130013826279675
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.04931

![](ETA_dAlpha_A.png)
## Feature : ETA_dAlpha_B
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 96
- **Count** :113.0
- **Mean** :0.025631238938053108
- **Std** :0.013575376812235617
- **Min** :0.0
- **25%th Percentile** : 0.01771
- **50%th Percentile** : 0.02571
- **75%th Percentile** : 0.035
- **Max** :0.06381

![](ETA_dAlpha_B.png)
## Feature : ETA_Epsilon_1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 110
- **Count** :113.0
- **Mean** :0.6348165486725665
- **Std** :0.05228465395194695
- **Min** :0.48073
- **25%th Percentile** : 0.59864
- **50%th Percentile** : 0.63333
- **75%th Percentile** : 0.66667
- **Max** :0.78076

![](ETA_Epsilon_1.png)
## Feature : ETA_Epsilon_2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 109
- **Count** :113.0
- **Mean** :0.8521246017699117
- **Std** :0.05519973723791302
- **Min** :0.74444
- **25%th Percentile** : 0.81111
- **50%th Percentile** : 0.84583
- **75%th Percentile** : 0.89167
- **Max** :1.00381

![](ETA_Epsilon_2.png)
## Feature : ETA_Epsilon_3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 44
- **Count** :113.0
- **Mean** :0.44222628318584073
- **Std** :0.003486964038358305
- **Min** :0.43333
- **25%th Percentile** : 0.44026
- **50%th Percentile** : 0.44237
- **75%th Percentile** : 0.44444
- **Max** :0.45

![](ETA_Epsilon_3.png)
## Feature : ETA_Epsilon_4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :0.5505838053097347
- **Std** :0.044063652524310624
- **Min** :0.46524
- **25%th Percentile** : 0.51984
- **50%th Percentile** : 0.54438
- **75%th Percentile** : 0.58118
- **Max** :0.67043

![](ETA_Epsilon_4.png)
## Feature : ETA_Epsilon_5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 104
- **Count** :113.0
- **Mean** :0.8131503539823008
- **Std** :0.05448528756084402
- **Min** :0.68519
- **25%th Percentile** : 0.7718
- **50%th Percentile** : 0.8098
- **75%th Percentile** : 0.85556
- **Max** :0.9788

![](ETA_Epsilon_5.png)
## Feature : ETA_dEpsilon_A
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.19259053097345133
- **Std** :0.05303038033996422
- **Min** :0.0474
- **25%th Percentile** : 0.15851
- **50%th Percentile** : 0.19096
- **75%th Percentile** : 0.22321
- **Max** :0.3423

![](ETA_dEpsilon_A.png)
## Feature : ETA_dEpsilon_B
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.08423247787610622
- **Std** :0.024633540851382654
- **Min** :0.00896
- **25%th Percentile** : 0.06778
- **50%th Percentile** : 0.08632
- **75%th Percentile** : 0.10314
- **Max** :0.14417

![](ETA_dEpsilon_B.png)
## Feature : ETA_dEpsilon_C
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :-0.10835778761061951
- **Std** :0.045642459159179236
- **Min** :-0.2325
- **25%th Percentile** : -0.14142
- **50%th Percentile** : -0.10245
- **75%th Percentile** : -0.07649
- **Max** :-0.02446

![](ETA_dEpsilon_C.png)
## Feature : ETA_dEpsilon_D
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 88
- **Count** :113.0
- **Mean** :0.038973893805309724
- **Std** :0.030327110026510132
- **Min** :0.0
- **25%th Percentile** : 0.01808
- **50%th Percentile** : 0.03508
- **75%th Percentile** : 0.05987
- **Max** :0.11696

![](ETA_dEpsilon_D.png)
## Feature : ETA_Psi_1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :0.5615546017699116
- **Std** :0.050477724047972
- **Min** :0.43453
- **25%th Percentile** : 0.52187
- **50%th Percentile** : 0.5625
- **75%th Percentile** : 0.59537
- **Max** :0.73787

![](ETA_Psi_1.png)
## Feature : ETA_dPsi_A
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :0.1529440707964602
- **Std** :0.04978634097386925
- **Min** :0.0
- **25%th Percentile** : 0.11892
- **50%th Percentile** : 0.15179
- **75%th Percentile** : 0.19242
- **Max** :0.27976

![](ETA_dPsi_A.png)
## Feature : ETA_dPsi_B
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.00020867256637168143
- **Std** :0.002218219807648523
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.02358

![](ETA_dPsi_B.png)
## Feature : ETA_Shape_P
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :0.1464100884955752
- **Std** :0.06870320818688651
- **Min** :0.0
- **25%th Percentile** : 0.10429
- **50%th Percentile** : 0.14706
- **75%th Percentile** : 0.18918
- **Max** :0.31567

![](ETA_Shape_P.png)
## Feature : ETA_Shape_Y
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 109
- **Count** :113.0
- **Mean** :0.3480835398230087
- **Std** :0.07365549213253789
- **Min** :0.07418
- **25%th Percentile** : 0.29565
- **50%th Percentile** : 0.34264
- **75%th Percentile** : 0.39494
- **Max** :0.54608

![](ETA_Shape_Y.png)
## Feature : ETA_Shape_X
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 20
- **Count** :113.0
- **Mean** :0.009622743362831858
- **Std** :0.022881154880310645
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.09485

![](ETA_Shape_X.png)
## Feature : ETA_Beta
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 71
- **Count** :113.0
- **Mean** :33.57079646017699
- **Std** :8.719492033614863
- **Min** :13.25
- **25%th Percentile** : 28.0
- **50%th Percentile** : 33.25
- **75%th Percentile** : 38.0
- **Max** :72.5

![](ETA_Beta.png)
## Feature : ETA_BetaP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 92
- **Count** :113.0
- **Mean** :1.37758017699115
- **Std** :0.16211004471710777
- **Min** :0.77778
- **25%th Percentile** : 1.29808
- **50%th Percentile** : 1.40278
- **75%th Percentile** : 1.48611
- **Max** :1.69444

![](ETA_BetaP.png)
## Feature : ETA_Beta_s
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 51
- **Count** :113.0
- **Mean** :15.676991150442477
- **Std** :4.334980409610487
- **Min** :6.25
- **25%th Percentile** : 13.0
- **50%th Percentile** : 15.5
- **75%th Percentile** : 17.75
- **Max** :40.5

![](ETA_Beta_s.png)
## Feature : ETA_BetaP_s
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 69
- **Count** :113.0
- **Mean** :0.6381798230088499
- **Std** :0.0235297602965107
- **Min** :0.55
- **25%th Percentile** : 0.625
- **50%th Percentile** : 0.63889
- **75%th Percentile** : 0.65625
- **Max** :0.69444

![](ETA_BetaP_s.png)
## Feature : ETA_Beta_ns
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 45
- **Count** :113.0
- **Mean** :17.893805309734514
- **Std** :5.164569080105895
- **Min** :2.5
- **25%th Percentile** : 15.0
- **50%th Percentile** : 17.5
- **75%th Percentile** : 20.5
- **Max** :32.25

![](ETA_Beta_ns.png)
## Feature : ETA_BetaP_ns
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 94
- **Count** :113.0
- **Mean** :0.7393999999999997
- **Std** :0.15817399119662953
- **Min** :0.13889
- **25%th Percentile** : 0.66667
- **50%th Percentile** : 0.76923
- **75%th Percentile** : 0.85294
- **Max** :1.0

![](ETA_BetaP_ns.png)
## Feature : ETA_dBeta
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 52
- **Count** :113.0
- **Mean** :2.2168141592920354
- **Std** :3.8600669094605102
- **Min** :-9.75
- **25%th Percentile** : 0.75
- **50%th Percentile** : 3.0
- **75%th Percentile** : 5.0
- **Max** :9.75

![](ETA_dBeta.png)
## Feature : ETA_dBetaP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 103
- **Count** :113.0
- **Mean** :0.10122008849557522
- **Std** :0.15768957505568365
- **Min** :-0.5
- **25%th Percentile** : 0.02586
- **50%th Percentile** : 0.13793
- **75%th Percentile** : 0.21739
- **Max** :0.34211

![](ETA_dBetaP.png)
## Feature : ETA_Beta_ns_d
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 6
- **Count** :113.0
- **Mean** :0.6946902654867256
- **Std** :0.5992317563771311
- **Min** :0.0
- **25%th Percentile** : 0.5
- **50%th Percentile** : 0.5
- **75%th Percentile** : 1.0
- **Max** :2.5

![](ETA_Beta_ns_d.png)
## Feature : ETA_BetaP_ns_d
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 49
- **Count** :113.0
- **Mean** :0.028970619469026567
- **Std** :0.02506310376127718
- **Min** :0.0
- **25%th Percentile** : 0.01563
- **50%th Percentile** : 0.02381
- **75%th Percentile** : 0.04054
- **Max** :0.11111

![](ETA_BetaP_ns_d.png)
## Feature : ETA_Eta
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :18.310077787610624
- **Std** :9.27263081256664
- **Min** :5.82359
- **25%th Percentile** : 12.4242
- **50%th Percentile** : 16.85502
- **75%th Percentile** : 22.315
- **Max** :80.84635

![](ETA_Eta.png)
## Feature : ETA_EtaP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :0.7174889380530972
- **Std** :0.16282512119362708
- **Min** :0.44248
- **25%th Percentile** : 0.59107
- **50%th Percentile** : 0.68221
- **75%th Percentile** : 0.80515
- **Max** :1.30397

![](ETA_EtaP.png)
## Feature : ETA_Eta_R
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :43.7496868141593
- **Std** :19.255426407753045
- **Min** :13.67595
- **25%th Percentile** : 31.33746
- **50%th Percentile** : 39.68782
- **75%th Percentile** : 52.75536
- **Max** :180.0074

![](ETA_Eta_R.png)
## Feature : ETA_Eta_F
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :25.43960929203541
- **Std** :10.788632671326756
- **Min** :7.00105
- **25%th Percentile** : 19.60723
- **50%th Percentile** : 24.1799
- **75%th Percentile** : 29.67157
- **Max** :99.16105

![](ETA_Eta_F.png)
## Feature : ETA_EtaP_F
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :1.0102478761061946
- **Std** :0.15626506081958566
- **Min** :0.40233
- **25%th Percentile** : 0.93918
- **50%th Percentile** : 1.00932
- **75%th Percentile** : 1.08167
- **Max** :1.59937

![](ETA_EtaP_F.png)
## Feature : ETA_Eta_L
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :5.110735929203542
- **Std** :1.8139506881373575
- **Min** :2.16187
- **25%th Percentile** : 3.80933
- **50%th Percentile** : 4.94396
- **75%th Percentile** : 5.9201
- **Max** :14.76779

![](ETA_Eta_L.png)
## Feature : ETA_EtaP_L
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :0.20643964601769915
- **Std** :0.035962138943249435
- **Min** :0.15896
- **25%th Percentile** : 0.18089
- **50%th Percentile** : 0.19609
- **75%th Percentile** : 0.22566
- **Max** :0.37113

![](ETA_EtaP_L.png)
## Feature : ETA_Eta_R_L
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 109
- **Count** :113.0
- **Mean** :11.820509380530972
- **Std** :3.142555841608434
- **Min** :5.23636
- **25%th Percentile** : 9.73715
- **50%th Percentile** : 11.66899
- **75%th Percentile** : 13.42218
- **Max** :29.4784

![](ETA_Eta_R_L.png)
## Feature : ETA_Eta_F_L
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :6.7097743362831865
- **Std** :1.7799953793704457
- **Min** :2.43084
- **25%th Percentile** : 5.51442
- **50%th Percentile** : 6.70437
- **75%th Percentile** : 7.59736
- **Max** :14.71061

![](ETA_Eta_F_L.png)
## Feature : ETA_EtaP_F_L
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :0.27571247787610614
- **Std** :0.03749327645751429
- **Min** :0.12342
- **25%th Percentile** : 0.25709
- **50%th Percentile** : 0.28749
- **75%th Percentile** : 0.30075
- **Max** :0.32995

![](ETA_EtaP_F_L.png)
## Feature : ETA_Eta_B
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 94
- **Count** :113.0
- **Mean** :0.3589773451327435
- **Std** :0.23421181340302116
- **Min** :0.01506
- **25%th Percentile** : 0.17686
- **50%th Percentile** : 0.31748
- **75%th Percentile** : 0.49181
- **Max** :1.4356

![](ETA_Eta_B.png)
## Feature : ETA_EtaP_B
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 102
- **Count** :113.0
- **Mean** :0.014109203539823006
- **Std** :0.006993519467232416
- **Min** :0.00072
- **25%th Percentile** : 0.00895
- **50%th Percentile** : 0.01347
- **75%th Percentile** : 0.01779
- **Max** :0.03273

![](ETA_EtaP_B.png)
## Feature : ETA_Eta_B_RC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 94
- **Count** :113.0
- **Mean** :0.6405702654867257
- **Std** :0.2690106733525781
- **Min** :0.13631
- **25%th Percentile** : 0.44918
- **50%th Percentile** : 0.59233
- **75%th Percentile** : 0.74779
- **Max** :2.1236

![](ETA_Eta_B_RC.png)
## Feature : ETA_EtaP_B_RC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 107
- **Count** :113.0
- **Mean** :0.025752566371681423
- **Std** :0.005941203849083033
- **Min** :0.00545
- **25%th Percentile** : 0.02174
- **50%th Percentile** : 0.02514
- **75%th Percentile** : 0.02991
- **Max** :0.04348

![](ETA_EtaP_B_RC.png)
## Feature : FMF
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 89
- **Count** :113.0
- **Mean** :0.46437624284204554
- **Std** :0.104055759695041
- **Min** :0.09375
- **25%th Percentile** : 0.419354838709677
- **50%th Percentile** : 0.466666666666667
- **75%th Percentile** : 0.534883720930233
- **Max** :0.642857142857143

![](FMF.png)
## Feature : fragC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 93
- **Count** :113.0
- **Mean** :153.1625663716814
- **Std** :108.98332359961874
- **Min** :11.02
- **25%th Percentile** : 94.04
- **50%th Percentile** : 134.1
- **75%th Percentile** : 191.07
- **Max** :979.2

![](fragC.png)
## Feature : nHBAcc
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :4.3628318584070795
- **Std** :2.2680307179461683
- **Min** :0.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 4.0
- **75%th Percentile** : 6.0
- **Max** :16.0

![](nHBAcc.png)
## Feature : nHBAcc2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :5.557522123893805
- **Std** :2.559778356397515
- **Min** :1.0
- **25%th Percentile** : 4.0
- **50%th Percentile** : 5.0
- **75%th Percentile** : 7.0
- **Max** :20.0

![](nHBAcc2.png)
## Feature : nHBAcc3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :4.938053097345133
- **Std** :2.4248828610909516
- **Min** :1.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 5.0
- **75%th Percentile** : 6.0
- **Max** :20.0

![](nHBAcc3.png)
## Feature : nHBAcc_Lipinski
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :5.876106194690266
- **Std** :2.496900733777032
- **Min** :2.0
- **25%th Percentile** : 4.0
- **50%th Percentile** : 5.0
- **75%th Percentile** : 7.0
- **Max** :20.0

![](nHBAcc_Lipinski.png)
## Feature : nHBDon
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :1.7079646017699115
- **Std** :1.5736752701490335
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 2.0
- **Max** :12.0

![](nHBDon.png)
## Feature : nHBDon_Lipinski
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :1.9026548672566372
- **Std** :1.7318683231552041
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 2.0
- **75%th Percentile** : 3.0
- **Max** :12.0

![](nHBDon_Lipinski.png)
## Feature : HybRatio
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 52
- **Count** :113.0
- **Mean** :0.17884723672423444
- **Std** :0.16083374612465012
- **Min** :0.0
- **25%th Percentile** : 0.0526315789473684
- **50%th Percentile** : 0.142857142857143
- **75%th Percentile** : 0.277777777777778
- **Max** :0.785714285714286

![](HybRatio.png)
## Feature : Kier1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 57
- **Count** :113.0
- **Mean** :18.91958867042434
- **Std** :5.501946307909142
- **Min** :8.59171597633136
- **25%th Percentile** : 15.39
- **50%th Percentile** : 18.0555555555556
- **75%th Percentile** : 22.203125
- **Max** :48.4566267590842

![](Kier1.png)
## Feature : Kier2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 86
- **Count** :113.0
- **Mean** :8.423092680487182
- **Std** :2.741379299608084
- **Min** :3.39506172839506
- **25%th Percentile** : 6.622248661511
- **50%th Percentile** : 8.16326530612245
- **75%th Percentile** : 9.42768595041322
- **Max** :19.5443218227127

![](Kier2.png)
## Feature : Kier3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 101
- **Count** :113.0
- **Mean** :4.455665028217718
- **Std** :1.879266128940923
- **Min** :1.70132325141777
- **25%th Percentile** : 3.2
- **50%th Percentile** : 4.23140495867769
- **75%th Percentile** : 5.09342560553633
- **Max** :13.8121284185493

![](Kier3.png)
## Feature : nAtomLC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 12
- **Count** :113.0
- **Mean** :3.743362831858407
- **Std** :2.7703105842574876
- **Min** :0.0
- **25%th Percentile** : 2.0
- **50%th Percentile** : 3.0
- **75%th Percentile** : 5.0
- **Max** :16.0

![](nAtomLC.png)
## Feature : nAtomP
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 25
- **Count** :113.0
- **Mean** :17.628318584070797
- **Std** :5.698048992959729
- **Min** :4.0
- **25%th Percentile** : 13.0
- **50%th Percentile** : 18.0
- **75%th Percentile** : 21.0
- **Max** :35.0

![](nAtomP.png)
## Feature : nAtomLAC
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :1.9432124405549922
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 2.0
- **Max** :16.0

![](nAtomLAC.png)
## Feature : MLogP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 21
- **Count** :113.0
- **Mean** :2.72353982300885
- **Std** :0.504964497256247
- **Min** :1.68
- **25%th Percentile** : 2.45
- **50%th Percentile** : 2.67
- **75%th Percentile** : 3.0
- **Max** :4.65

![](MLogP.png)
## Feature : McGowan_Volume
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :2.4566008849557512
- **Std** :0.6439756717476073
- **Min** :1.2439
- **25%th Percentile** : 1.9856
- **50%th Percentile** : 2.4194
- **75%th Percentile** : 2.7764
- **Max** :5.64360000000001

![](McGowan_Volume.png)
## Feature : MDEC.11
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 25
- **Count** :113.0
- **Mean** :0.11650906786741723
- **Std** :0.2570729914191084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.111111111111111
- **Max** :1.5

![](MDEC.11.png)
## Feature : MDEC.12
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 75
- **Count** :113.0
- **Mean** :1.5785909338428175
- **Std** :1.475841151848211
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.48719007172661
- **75%th Percentile** : 2.55180933861595
- **Max** :5.98440366382444

![](MDEC.12.png)
## Feature : MDEC.13
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 73
- **Count** :113.0
- **Mean** :1.9587188920868197
- **Std** :2.1991561715976484
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.51967137130319
- **75%th Percentile** : 2.87580992156241
- **Max** :10.1190206521989

![](MDEC.13.png)
## Feature : MDEC.14
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :0.06581513287801746
- **Std** :0.3515249768467342
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.0

![](MDEC.14.png)
## Feature : MDEC.22
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :8.854146328468687
- **Std** :5.255293416941971
- **Min** :0.0
- **25%th Percentile** : 4.97319778183276
- **50%th Percentile** : 8.07427907839925
- **75%th Percentile** : 11.3453706197523
- **Max** :35.3375037610959

![](MDEC.22.png)
## Feature : MDEC.23
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :17.500742472782704
- **Std** :7.564721284960124
- **Min** :3.30763445773712
- **25%th Percentile** : 13.1525866455154
- **50%th Percentile** : 16.5733665783658
- **75%th Percentile** : 21.564741575182
- **Max** :63.0586286099019

![](MDEC.23.png)
## Feature : MDEC.24
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.17527266664003688
- **Std** :0.6890679263390079
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :4.1984166758498

![](MDEC.24.png)
## Feature : MDEC.33
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 99
- **Count** :113.0
- **Mean** :8.534765367389902
- **Std** :5.835217119520718
- **Min** :0.0
- **25%th Percentile** : 4.40144761435434
- **50%th Percentile** : 7.14906487873217
- **75%th Percentile** : 10.73130138275
- **Max** :34.9030842082333

![](MDEC.33.png)
## Feature : MDEC.34
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.23905759181628697
- **Std** :0.9337420212291236
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :6.23897999180166

![](MDEC.34.png)
## Feature : MDEC.44
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.0011061946902654867
- **Std** :0.011759010854794971
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.125

![](MDEC.44.png)
## Feature : MDEO.11
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 45
- **Count** :113.0
- **Mean** :0.5396221141708267
- **Std** :1.3059827092701208
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.142857142857143
- **75%th Percentile** : 0.550321208149104
- **Max** :11.8108290464854

![](MDEO.11.png)
## Feature : MDEO.12
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 41
- **Count** :113.0
- **Mean** :0.41817192774327705
- **Std** :0.8875594097010806
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.539404474380147
- **Max** :7.98282901980573

![](MDEO.12.png)
## Feature : MDEO.22
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 20
- **Count** :113.0
- **Mean** :0.12055131397166412
- **Std** :0.2725870498261404
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0909090909090909
- **Max** :1.64580173679795

![](MDEO.22.png)
## Feature : MDEN.11
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :0.009980334316617502
- **Std** :0.0394161345705231
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.25

![](MDEN.11.png)
## Feature : MDEN.12
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 13
- **Count** :113.0
- **Mean** :0.06875429462325612
- **Std** :0.2106673918286
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.14060729661474

![](MDEN.12.png)
## Feature : MDEN.13
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 10
- **Count** :113.0
- **Mean** :0.037092718728183746
- **Std** :0.1286114813149999
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.707106781186548

![](MDEN.13.png)
## Feature : MDEN.22
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 30
- **Count** :113.0
- **Mean** :0.593061590528917
- **Std** :0.8407195401649323
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.2
- **75%th Percentile** : 1.04004191152595
- **Max** :3.27449357625595

![](MDEN.22.png)
## Feature : MDEN.23
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 33
- **Count** :113.0
- **Mean** :0.27902530845229584
- **Std** :0.4330685524754143
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.5
- **Max** :1.88988157484231

![](MDEN.23.png)
## Feature : MDEN.33
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :0.033388472403226996
- **Std** :0.10779071822592262
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.550321208149104

![](MDEN.33.png)
## Feature : MLFER_A
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 66
- **Count** :113.0
- **Mean** :0.35247787610619447
- **Std** :0.44206930406379585
- **Min** :-0.334
- **25%th Percentile** : 0.003
- **50%th Percentile** : 0.25
- **75%th Percentile** : 0.572
- **Max** :2.715

![](MLFER_A.png)
## Feature : MLFER_BH
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :1.4923008849557522
- **Std** :0.6884444796184127
- **Min** :0.142
- **25%th Percentile** : 1.098
- **50%th Percentile** : 1.414
- **75%th Percentile** : 1.741
- **Max** :5.027

![](MLFER_BH.png)
## Feature : MLFER_BO
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 106
- **Count** :113.0
- **Mean** :1.4891150442477887
- **Std** :0.6936350773823519
- **Min** :0.127
- **25%th Percentile** : 1.074
- **50%th Percentile** : 1.418
- **75%th Percentile** : 1.756
- **Max** :5.122

![](MLFER_BO.png)
## Feature : MLFER_S
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 106
- **Count** :113.0
- **Mean** :2.7976548672566355
- **Std** :0.8517879909963291
- **Min** :1.1
- **25%th Percentile** : 2.271
- **50%th Percentile** : 2.678
- **75%th Percentile** : 3.322
- **Max** :6.103

![](MLFER_S.png)
## Feature : MLFER_E
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 108
- **Count** :113.0
- **Mean** :2.5149823008849572
- **Std** :0.7779172626578533
- **Min** :0.925
- **25%th Percentile** : 1.997
- **50%th Percentile** : 2.431
- **75%th Percentile** : 3.04
- **Max** :5.65

![](MLFER_E.png)
## Feature : MLFER_L
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :13.310336283185846
- **Std** :3.664676534182742
- **Min** :6.108
- **25%th Percentile** : 10.833
- **50%th Percentile** : 12.908
- **75%th Percentile** : 15.023
- **Max** :32.112

![](MLFER_L.png)
## Feature : PetitjeanNumber
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.47665626442442255
- **Std** :0.024816788720345467
- **Min** :0.4
- **25%th Percentile** : 0.461538461538462
- **50%th Percentile** : 0.473684210526316
- **75%th Percentile** : 0.5
- **Max** :0.5

![](PetitjeanNumber.png)
## Feature : nRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :3.274336283185841
- **Std** :1.0876562909910885
- **Min** :1.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 3.0
- **75%th Percentile** : 4.0
- **Max** :8.0

![](nRing.png)
## Feature : n3Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n3Ring.png)
## Feature : n4Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n4Ring.png)
## Feature : n5Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.6814159292035398
- **Std** :0.7707311779554312
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](n5Ring.png)
## Feature : n6Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :2.5929203539823007
- **Std** :1.0908480329999561
- **Min** :0.0
- **25%th Percentile** : 2.0
- **50%th Percentile** : 3.0
- **75%th Percentile** : 3.0
- **Max** :8.0

![](n6Ring.png)
## Feature : n7Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n7Ring.png)
## Feature : n8Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n8Ring.png)
## Feature : n9Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n9Ring.png)
## Feature : n10Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n10Ring.png)
## Feature : n11Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n11Ring.png)
## Feature : n12Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n12Ring.png)
## Feature : nG12Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nG12Ring.png)
## Feature : nFRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 8
- **Count** :113.0
- **Mean** :1.752212389380531
- **Std** :2.0853698432315118
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 3.0
- **Max** :8.0

![](nFRing.png)
## Feature : nF4Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF4Ring.png)
## Feature : nF5Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF5Ring.png)
## Feature : nF6Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.38705344156140187
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :4.0

![](nF6Ring.png)
## Feature : nF7Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF7Ring.png)
## Feature : nF8Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.29668416166447786
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.0

![](nF8Ring.png)
## Feature : nF9Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.6589843927102151
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nF9Ring.png)
## Feature : nF10Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :1.0311979213375695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :4.0

![](nF10Ring.png)
## Feature : nF11Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469427
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nF11Ring.png)
## Feature : nF12Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nF12Ring.png)
## Feature : nFG12Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.40707964601769914
- **Std** :0.830631946833177
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nFG12Ring.png)
## Feature : nTRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 14
- **Count** :113.0
- **Mean** :5.0265486725663715
- **Std** :2.7949577590443053
- **Min** :1.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 4.0
- **75%th Percentile** : 6.0
- **Max** :14.0

![](nTRing.png)
## Feature : nT4Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nT4Ring.png)
## Feature : nT5Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.6814159292035398
- **Std** :0.7707311779554312
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nT5Ring.png)
## Feature : nT6Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 7
- **Count** :113.0
- **Mean** :2.6371681415929205
- **Std** :1.0943193163418246
- **Min** :0.0
- **25%th Percentile** : 2.0
- **50%th Percentile** : 3.0
- **75%th Percentile** : 3.0
- **Max** :8.0

![](nT6Ring.png)
## Feature : nT7Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nT7Ring.png)
## Feature : nT8Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.29668416166447786
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :3.0

![](nT8Ring.png)
## Feature : nT9Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.6589843927102151
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nT9Ring.png)
## Feature : nT10Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 5
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :1.0311979213375695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :4.0

![](nT10Ring.png)
## Feature : nT11Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469427
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nT11Ring.png)
## Feature : nT12Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nT12Ring.png)
## Feature : nTG12Ring
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.40707964601769914
- **Std** :0.830631946833177
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nTG12Ring.png)
## Feature : nHeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :1.3097345132743363
- **Std** :0.7568690915505976
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 2.0
- **Max** :3.0

![](nHeteroRing.png)
## Feature : n3HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n3HeteroRing.png)
## Feature : n4HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n4HeteroRing.png)
## Feature : n5HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.6371681415929203
- **Std** :0.7683696434503245
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](n5HeteroRing.png)
## Feature : n6HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.672566371681416
- **Std** :0.6187902638574347
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](n6HeteroRing.png)
## Feature : n7HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n7HeteroRing.png)
## Feature : n8HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n8HeteroRing.png)
## Feature : n9HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n9HeteroRing.png)
## Feature : n10HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n10HeteroRing.png)
## Feature : n11HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n11HeteroRing.png)
## Feature : n12HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](n12HeteroRing.png)
## Feature : nG12HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nG12HeteroRing.png)
## Feature : nFHeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nFHeteroRing.png)
## Feature : nF4HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF4HeteroRing.png)
## Feature : nF5HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF5HeteroRing.png)
## Feature : nF6HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF6HeteroRing.png)
## Feature : nF7HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nF7HeteroRing.png)
## Feature : nF8HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835977
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nF8HeteroRing.png)
## Feature : nF9HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.36283185840707965
- **Std** :0.6417324056057562
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nF9HeteroRing.png)
## Feature : nF10HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.504424778761062
- **Std** :0.7334348472051333
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nF10HeteroRing.png)
## Feature : nF11HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nF11HeteroRing.png)
## Feature : nF12HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nF12HeteroRing.png)
## Feature : nFG12HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.547376226419317
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nFG12HeteroRing.png)
## Feature : nTHeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nTHeteroRing.png)
## Feature : nT4HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nT4HeteroRing.png)
## Feature : nT5HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 4
- **Count** :113.0
- **Mean** :0.6371681415929203
- **Std** :0.7683696434503245
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :3.0

![](nT5HeteroRing.png)
## Feature : nT6HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.672566371681416
- **Std** :0.6187902638574347
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nT6HeteroRing.png)
## Feature : nT7HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](nT7HeteroRing.png)
## Feature : nT8HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835977
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nT8HeteroRing.png)
## Feature : nT9HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.36283185840707965
- **Std** :0.6417324056057562
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nT9HeteroRing.png)
## Feature : nT10HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.504424778761062
- **Std** :0.7334348472051333
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](nT10HeteroRing.png)
## Feature : nT11HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nT11HeteroRing.png)
## Feature : nT12HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](nT12HeteroRing.png)
## Feature : nTG12HeteroRing
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.547376226419317
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :2.0

![](nTG12HeteroRing.png)
## Feature : nRotB
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 13
- **Count** :113.0
- **Mean** :4.584070796460177
- **Std** :2.8558388301013307
- **Min** :0.0
- **25%th Percentile** : 3.0
- **50%th Percentile** : 4.0
- **75%th Percentile** : 6.0
- **Max** :16.0

![](nRotB.png)
## Feature : RotBFrac
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 68
- **Count** :113.0
- **Mean** :0.1672799214084604
- **Std** :0.09927363641685466
- **Min** :0.0
- **25%th Percentile** : 0.0967741935483871
- **50%th Percentile** : 0.16
- **75%th Percentile** : 0.219512195121951
- **Max** :0.64

![](RotBFrac.png)
## Feature : nRotBt
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 17
- **Count** :113.0
- **Mean** :6.8584070796460175
- **Std** :3.6126446007569024
- **Min** :0.0
- **25%th Percentile** : 4.0
- **50%th Percentile** : 6.0
- **75%th Percentile** : 9.0
- **Max** :21.0

![](nRotBt.png)
## Feature : RotBtFrac
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 80
- **Count** :113.0
- **Mean** :0.25242534023801927
- **Std** :0.11531127640171246
- **Min** :0.0
- **25%th Percentile** : 0.171428571428571
- **50%th Percentile** : 0.241379310344828
- **75%th Percentile** : 0.3125
- **Max** :0.72

![](RotBtFrac.png)
## Feature : LipinskiFailures
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 3
- **Count** :113.0
- **Mean** :0.3185840707964602
- **Std** :0.522109162191198
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :2.0

![](LipinskiFailures.png)
## Feature : topoRadius
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 9
- **Count** :113.0
- **Mean** :6.460176991150442
- **Std** :1.7578162092600282
- **Min** :3.0
- **25%th Percentile** : 5.0
- **50%th Percentile** : 6.0
- **75%th Percentile** : 8.0
- **Max** :11.0

![](topoRadius.png)
## Feature : topoDiameter
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 17
- **Count** :113.0
- **Mean** :12.380530973451327
- **Std** :3.480349387404528
- **Min** :6.0
- **25%th Percentile** : 10.0
- **50%th Percentile** : 12.0
- **75%th Percentile** : 15.0
- **Max** :22.0

![](topoDiameter.png)
## Feature : topoShape
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 11
- **Count** :113.0
- **Mean** :0.9149561353101175
- **Std** :0.08872959873131037
- **Min** :0.666666666666667
- **25%th Percentile** : 0.857142857142857
- **50%th Percentile** : 0.9
- **75%th Percentile** : 1.0
- **Max** :1.0

![](topoShape.png)
## Feature : globalTopoChargeIndex
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :1.7210122221853592
- **Std** :0.11279759192304727
- **Min** :1.22020811315797
- **25%th Percentile** : 1.64815090797912
- **50%th Percentile** : 1.72646535319781
- **75%th Percentile** : 1.78523986678005
- **Max** :2.09789648946558

![](globalTopoChargeIndex.png)
## Feature : TopoPSA
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 99
- **Count** :113.0
- **Mean** :90.10831858407073
- **Std** :45.219193813157304
- **Min** :20.31
- **25%th Percentile** : 58.2
- **50%th Percentile** : 80.39
- **75%th Percentile** : 109.88
- **Max** :347.96

![](TopoPSA.png)
## Feature : VABC
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :297.68455641060757
- **Std** :83.33400917775164
- **Min** :137.5153900525
- **25%th Percentile** : 240.818035270968
- **50%th Percentile** : 287.440501760645
- **75%th Percentile** : 341.915991637768
- **Max** :726.157338179221

![](VABC.png)
## Feature : VAdjMat
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 27
- **Count** :113.0
- **Mean** :5.56982347397433
- **Std** :0.3657695652387953
- **Min** :4.4594316186373
- **25%th Percentile** : 5.32192809488736
- **50%th Percentile** : 5.58496250072116
- **75%th Percentile** : 5.8073549220576
- **Max** :6.95419631038688

![](VAdjMat.png)
## Feature : MW
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :344.6673324216641
- **Std** :94.19211333873936
- **Min** :151.099714036
- **25%th Percentile** : 287.986861044
- **50%th Percentile** : 328.070405132
- **75%th Percentile** : 400.138284360001
- **Max** :862.195643616001

![](MW.png)
## Feature : WTPT.1
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 112
- **Count** :113.0
- **Mean** :50.07358427508075
- **Std** :13.554773222467754
- **Min** :21.405456471894
- **25%th Percentile** : 41.05966692985
- **50%th Percentile** : 49.5986551146904
- **75%th Percentile** : 57.4632348059767
- **Max** :127.223508790467

![](WTPT.1.png)
## Feature : WTPT.2
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :2.0405337086442445
- **Std** :0.037066277975806035
- **Min** :1.94011242332837
- **25%th Percentile** : 2.01859848804896
- **50%th Percentile** : 2.04329600157089
- **75%th Percentile** : 2.06520949103491
- **Max** :2.11959502354934

![](WTPT.2.png)
## Feature : WTPT.3
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :18.561416652649392
- **Std** :7.536657878363101
- **Min** :5.42339616830319
- **25%th Percentile** : 14.0005140790465
- **50%th Percentile** : 17.0438320485947
- **75%th Percentile** : 22.5279888517601
- **Max** :52.779784475426

![](WTPT.3.png)
## Feature : WTPT.4
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 101
- **Count** :113.0
- **Mean** :8.443330704926645
- **Std** :6.648982870351966
- **Min** :0.0
- **25%th Percentile** : 5.12383078640366
- **50%th Percentile** : 7.72104799055402
- **75%th Percentile** : 11.1868897011502
- **Max** :52.779784475426

![](WTPT.4.png)
## Feature : WTPT.5
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 101
- **Count** :113.0
- **Mean** :8.307050700240666
- **Std** :4.65145213231728
- **Min** :0.0
- **25%th Percentile** : 6.02071243289152
- **50%th Percentile** : 8.49138405754604
- **75%th Percentile** : 12.0070901923051
- **Max** :18.4438122865405

![](WTPT.5.png)
## Feature : WPATH
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 111
- **Count** :113.0
- **Mean** :1739.6106194690265
- **Std** :1656.2070502982556
- **Min** :156.0
- **25%th Percentile** : 839.0
- **50%th Percentile** : 1356.0
- **75%th Percentile** : 2124.0
- **Max** :14775.0

![](WPATH.png)
## Feature : WPOL
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 43
- **Count** :113.0
- **Mean** :39.61946902654867
- **Std** :14.447144171619176
- **Min** :13.0
- **25%th Percentile** : 30.0
- **50%th Percentile** : 38.0
- **75%th Percentile** : 46.0
- **Max** :132.0

![](WPOL.png)
## Feature : XLogP
- **Feature type** : continous
- **Missing** : 0.0%
- **Unique** : 113
- **Count** :113.0
- **Mean** :3.8920530973451317
- **Std** :1.9861700214982494
- **Min** :-0.205
- **25%th Percentile** : 2.656
- **50%th Percentile** : 3.871
- **75%th Percentile** : 5.158
- **Max** :12.837

![](XLogP.png)
## Feature : Zagreb
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 52
- **Count** :113.0
- **Mean** :129.61061946902655
- **Std** :37.425028301429926
- **Min** :50.0
- **25%th Percentile** : 104.0
- **50%th Percentile** : 126.0
- **75%th Percentile** : 150.0
- **Max** :350.0

![](Zagreb.png)
## Feature : PubchemFP0
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP0.png)
## Feature : PubchemFP1
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.1324427696454308
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP1.png)
## Feature : PubchemFP2
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5309734513274337
- **Std** :0.5012626282730048
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP2.png)
## Feature : PubchemFP3
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.1614762803872026
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP3.png)
## Feature : PubchemFP4
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP4.png)
## Feature : PubchemFP5
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP5.png)
## Feature : PubchemFP6
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP6.png)
## Feature : PubchemFP7
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP7.png)
## Feature : PubchemFP8
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP8.png)
## Feature : PubchemFP9
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP9.png)
## Feature : PubchemFP10
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP10.png)
## Feature : PubchemFP11
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP11.png)
## Feature : PubchemFP12
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6548672566371682
- **Std** :0.4775291970210677
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP12.png)
## Feature : PubchemFP13
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543103
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP13.png)
## Feature : PubchemFP14
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9026548672566371
- **Std** :0.29774754916562113
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP14.png)
## Feature : PubchemFP15
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.39071072068300994
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP15.png)
## Feature : PubchemFP16
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3008849557522124
- **Std** :0.46068574182199146
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP16.png)
## Feature : PubchemFP17
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP17.png)
## Feature : PubchemFP18
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8849557522123894
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP18.png)
## Feature : PubchemFP19
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.39071072068301
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP19.png)
## Feature : PubchemFP20
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP20.png)
## Feature : PubchemFP21
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP21.png)
## Feature : PubchemFP22
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP22.png)
## Feature : PubchemFP23
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487176
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP23.png)
## Feature : PubchemFP24
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720252
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP24.png)
## Feature : PubchemFP25
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP25.png)
## Feature : PubchemFP26
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP26.png)
## Feature : PubchemFP27
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP27.png)
## Feature : PubchemFP28
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP28.png)
## Feature : PubchemFP29
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP29.png)
## Feature : PubchemFP30
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP30.png)
## Feature : PubchemFP31
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP31.png)
## Feature : PubchemFP32
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP32.png)
## Feature : PubchemFP33
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3008849557522124
- **Std** :0.46068574182199146
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP33.png)
## Feature : PubchemFP34
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP34.png)
## Feature : PubchemFP35
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP35.png)
## Feature : PubchemFP36
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP36.png)
## Feature : PubchemFP37
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP37.png)
## Feature : PubchemFP38
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP38.png)
## Feature : PubchemFP39
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP39.png)
## Feature : PubchemFP40
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP40.png)
## Feature : PubchemFP41
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP41.png)
## Feature : PubchemFP42
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP42.png)
## Feature : PubchemFP43
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP43.png)
## Feature : PubchemFP44
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP44.png)
## Feature : PubchemFP45
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP45.png)
## Feature : PubchemFP46
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP46.png)
## Feature : PubchemFP47
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP47.png)
## Feature : PubchemFP48
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP48.png)
## Feature : PubchemFP49
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP49.png)
## Feature : PubchemFP50
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP50.png)
## Feature : PubchemFP51
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP51.png)
## Feature : PubchemFP52
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP52.png)
## Feature : PubchemFP53
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP53.png)
## Feature : PubchemFP54
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP54.png)
## Feature : PubchemFP55
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP55.png)
## Feature : PubchemFP56
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP56.png)
## Feature : PubchemFP57
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP57.png)
## Feature : PubchemFP58
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP58.png)
## Feature : PubchemFP59
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP59.png)
## Feature : PubchemFP60
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP60.png)
## Feature : PubchemFP61
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP61.png)
## Feature : PubchemFP62
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP62.png)
## Feature : PubchemFP63
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP63.png)
## Feature : PubchemFP64
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP64.png)
## Feature : PubchemFP65
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP65.png)
## Feature : PubchemFP66
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP66.png)
## Feature : PubchemFP67
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP67.png)
## Feature : PubchemFP68
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP68.png)
## Feature : PubchemFP69
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP69.png)
## Feature : PubchemFP70
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP70.png)
## Feature : PubchemFP71
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP71.png)
## Feature : PubchemFP72
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP72.png)
## Feature : PubchemFP73
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP73.png)
## Feature : PubchemFP74
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP74.png)
## Feature : PubchemFP75
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP75.png)
## Feature : PubchemFP76
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP76.png)
## Feature : PubchemFP77
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP77.png)
## Feature : PubchemFP78
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP78.png)
## Feature : PubchemFP79
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP79.png)
## Feature : PubchemFP80
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP80.png)
## Feature : PubchemFP81
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP81.png)
## Feature : PubchemFP82
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP82.png)
## Feature : PubchemFP83
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP83.png)
## Feature : PubchemFP84
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP84.png)
## Feature : PubchemFP85
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP85.png)
## Feature : PubchemFP86
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP86.png)
## Feature : PubchemFP87
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP87.png)
## Feature : PubchemFP88
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP88.png)
## Feature : PubchemFP89
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP89.png)
## Feature : PubchemFP90
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP90.png)
## Feature : PubchemFP91
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP91.png)
## Feature : PubchemFP92
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP92.png)
## Feature : PubchemFP93
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP93.png)
## Feature : PubchemFP94
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP94.png)
## Feature : PubchemFP95
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP95.png)
## Feature : PubchemFP96
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP96.png)
## Feature : PubchemFP97
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP97.png)
## Feature : PubchemFP98
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP98.png)
## Feature : PubchemFP99
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP99.png)
## Feature : PubchemFP100
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP100.png)
## Feature : PubchemFP101
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP101.png)
## Feature : PubchemFP102
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP102.png)
## Feature : PubchemFP103
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP103.png)
## Feature : PubchemFP104
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP104.png)
## Feature : PubchemFP105
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP105.png)
## Feature : PubchemFP106
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP106.png)
## Feature : PubchemFP107
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP107.png)
## Feature : PubchemFP108
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP108.png)
## Feature : PubchemFP109
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP109.png)
## Feature : PubchemFP110
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP110.png)
## Feature : PubchemFP111
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP111.png)
## Feature : PubchemFP112
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP112.png)
## Feature : PubchemFP113
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP113.png)
## Feature : PubchemFP114
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP114.png)
## Feature : PubchemFP115
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP115.png)
## Feature : PubchemFP116
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP116.png)
## Feature : PubchemFP117
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP117.png)
## Feature : PubchemFP118
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP118.png)
## Feature : PubchemFP119
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP119.png)
## Feature : PubchemFP120
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP120.png)
## Feature : PubchemFP121
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP121.png)
## Feature : PubchemFP122
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP122.png)
## Feature : PubchemFP123
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP123.png)
## Feature : PubchemFP124
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP124.png)
## Feature : PubchemFP125
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP125.png)
## Feature : PubchemFP126
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP126.png)
## Feature : PubchemFP127
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP127.png)
## Feature : PubchemFP128
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP128.png)
## Feature : PubchemFP129
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP129.png)
## Feature : PubchemFP130
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP130.png)
## Feature : PubchemFP131
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP131.png)
## Feature : PubchemFP132
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP132.png)
## Feature : PubchemFP133
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP133.png)
## Feature : PubchemFP134
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP134.png)
## Feature : PubchemFP135
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP135.png)
## Feature : PubchemFP136
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP136.png)
## Feature : PubchemFP137
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP137.png)
## Feature : PubchemFP138
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP138.png)
## Feature : PubchemFP139
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP139.png)
## Feature : PubchemFP140
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP140.png)
## Feature : PubchemFP141
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP141.png)
## Feature : PubchemFP142
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP142.png)
## Feature : PubchemFP143
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.504424778761062
- **Std** :0.5022075162525258
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP143.png)
## Feature : PubchemFP144
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720266
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP144.png)
## Feature : PubchemFP145
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :0.47136727180667504
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP145.png)
## Feature : PubchemFP146
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4424778761061947
- **Std** :0.4988925789283045
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP146.png)
## Feature : PubchemFP147
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543092
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP147.png)
## Feature : PubchemFP148
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720244
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP148.png)
## Feature : PubchemFP149
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720244
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP149.png)
## Feature : PubchemFP150
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935886
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP150.png)
## Feature : PubchemFP151
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP151.png)
## Feature : PubchemFP152
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP152.png)
## Feature : PubchemFP153
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1504424778761062
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP153.png)
## Feature : PubchemFP154
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP154.png)
## Feature : PubchemFP155
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835981
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP155.png)
## Feature : PubchemFP156
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835981
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP156.png)
## Feature : PubchemFP157
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP157.png)
## Feature : PubchemFP158
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP158.png)
## Feature : PubchemFP159
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP159.png)
## Feature : PubchemFP160
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP160.png)
## Feature : PubchemFP161
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP161.png)
## Feature : PubchemFP162
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP162.png)
## Feature : PubchemFP163
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP163.png)
## Feature : PubchemFP164
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP164.png)
## Feature : PubchemFP165
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP165.png)
## Feature : PubchemFP166
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP166.png)
## Feature : PubchemFP167
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP167.png)
## Feature : PubchemFP168
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP168.png)
## Feature : PubchemFP169
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP169.png)
## Feature : PubchemFP170
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP170.png)
## Feature : PubchemFP171
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP171.png)
## Feature : PubchemFP172
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP172.png)
## Feature : PubchemFP173
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP173.png)
## Feature : PubchemFP174
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP174.png)
## Feature : PubchemFP175
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP175.png)
## Feature : PubchemFP176
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP176.png)
## Feature : PubchemFP177
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP177.png)
## Feature : PubchemFP178
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :0.16147628038720235
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP178.png)
## Feature : PubchemFP179
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9469026548672567
- **Std** :0.2252263926348718
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP179.png)
## Feature : PubchemFP180
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2920353982300885
- **Std** :0.45672389145028514
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP180.png)
## Feature : PubchemFP181
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.35398230088495575
- **Std** :0.4803338493452255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP181.png)
## Feature : PubchemFP182
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487185
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP182.png)
## Feature : PubchemFP183
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935875
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP183.png)
## Feature : PubchemFP184
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.43365089339828694
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP184.png)
## Feature : PubchemFP185
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8938053097345132
- **Std** :0.30945897080939894
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP185.png)
## Feature : PubchemFP186
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.672566371681416
- **Std** :0.47136727180667515
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP186.png)
## Feature : PubchemFP187
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720271
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP187.png)
## Feature : PubchemFP188
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487162
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP188.png)
## Feature : PubchemFP189
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835985
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP189.png)
## Feature : PubchemFP190
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.132442769645431
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP190.png)
## Feature : PubchemFP191
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.132442769645431
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP191.png)
## Feature : PubchemFP192
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5486725663716814
- **Std** :0.49984194720641933
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP192.png)
## Feature : PubchemFP193
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.20353982300884957
- **Std** :0.40442401801376365
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP193.png)
## Feature : PubchemFP194
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP194.png)
## Feature : PubchemFP195
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP195.png)
## Feature : PubchemFP196
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP196.png)
## Feature : PubchemFP197
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP197.png)
## Feature : PubchemFP198
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP198.png)
## Feature : PubchemFP199
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1504424778761062
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP199.png)
## Feature : PubchemFP200
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.2065611519179459
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP200.png)
## Feature : PubchemFP201
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP201.png)
## Feature : PubchemFP202
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP202.png)
## Feature : PubchemFP203
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP203.png)
## Feature : PubchemFP204
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP204.png)
## Feature : PubchemFP205
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP205.png)
## Feature : PubchemFP206
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP206.png)
## Feature : PubchemFP207
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP207.png)
## Feature : PubchemFP208
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP208.png)
## Feature : PubchemFP209
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP209.png)
## Feature : PubchemFP210
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP210.png)
## Feature : PubchemFP211
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP211.png)
## Feature : PubchemFP212
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP212.png)
## Feature : PubchemFP213
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP213.png)
## Feature : PubchemFP214
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP214.png)
## Feature : PubchemFP215
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP215.png)
## Feature : PubchemFP216
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP216.png)
## Feature : PubchemFP217
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP217.png)
## Feature : PubchemFP218
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP218.png)
## Feature : PubchemFP219
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP219.png)
## Feature : PubchemFP220
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP220.png)
## Feature : PubchemFP221
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP221.png)
## Feature : PubchemFP222
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP222.png)
## Feature : PubchemFP223
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP223.png)
## Feature : PubchemFP224
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP224.png)
## Feature : PubchemFP225
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP225.png)
## Feature : PubchemFP226
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP226.png)
## Feature : PubchemFP227
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP227.png)
## Feature : PubchemFP228
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP228.png)
## Feature : PubchemFP229
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP229.png)
## Feature : PubchemFP230
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP230.png)
## Feature : PubchemFP231
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP231.png)
## Feature : PubchemFP232
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP232.png)
## Feature : PubchemFP233
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP233.png)
## Feature : PubchemFP234
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP234.png)
## Feature : PubchemFP235
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP235.png)
## Feature : PubchemFP236
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP236.png)
## Feature : PubchemFP237
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP237.png)
## Feature : PubchemFP238
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP238.png)
## Feature : PubchemFP239
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP239.png)
## Feature : PubchemFP240
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP240.png)
## Feature : PubchemFP241
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP241.png)
## Feature : PubchemFP242
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP242.png)
## Feature : PubchemFP243
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP243.png)
## Feature : PubchemFP244
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP244.png)
## Feature : PubchemFP245
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP245.png)
## Feature : PubchemFP246
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP246.png)
## Feature : PubchemFP247
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP247.png)
## Feature : PubchemFP248
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP248.png)
## Feature : PubchemFP249
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP249.png)
## Feature : PubchemFP250
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP250.png)
## Feature : PubchemFP251
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP251.png)
## Feature : PubchemFP252
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP252.png)
## Feature : PubchemFP253
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP253.png)
## Feature : PubchemFP254
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP254.png)
## Feature : PubchemFP255
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP255.png)
## Feature : PubchemFP256
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5309734513274337
- **Std** :0.5012626282730048
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP256.png)
## Feature : PubchemFP257
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8584070796460177
- **Std** :0.35018506936557714
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP257.png)
## Feature : PubchemFP258
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP258.png)
## Feature : PubchemFP259
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3893805309734513
- **Std** :0.48978180853796094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP259.png)
## Feature : PubchemFP260
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP260.png)
## Feature : PubchemFP261
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.18584070796460178
- **Std** :0.39071072068300994
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP261.png)
## Feature : PubchemFP262
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP262.png)
## Feature : PubchemFP263
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP263.png)
## Feature : PubchemFP264
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP264.png)
## Feature : PubchemFP265
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP265.png)
## Feature : PubchemFP266
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP266.png)
## Feature : PubchemFP267
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP267.png)
## Feature : PubchemFP268
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP268.png)
## Feature : PubchemFP269
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP269.png)
## Feature : PubchemFP270
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP270.png)
## Feature : PubchemFP271
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP271.png)
## Feature : PubchemFP272
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP272.png)
## Feature : PubchemFP273
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP273.png)
## Feature : PubchemFP274
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP274.png)
## Feature : PubchemFP275
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP275.png)
## Feature : PubchemFP276
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP276.png)
## Feature : PubchemFP277
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP277.png)
## Feature : PubchemFP278
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP278.png)
## Feature : PubchemFP279
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP279.png)
## Feature : PubchemFP280
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP280.png)
## Feature : PubchemFP281
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP281.png)
## Feature : PubchemFP282
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP282.png)
## Feature : PubchemFP283
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP283.png)
## Feature : PubchemFP284
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP284.png)
## Feature : PubchemFP285
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9026548672566371
- **Std** :0.29774754916562113
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP285.png)
## Feature : PubchemFP286
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8672566371681416
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP286.png)
## Feature : PubchemFP287
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487176
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP287.png)
## Feature : PubchemFP288
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP288.png)
## Feature : PubchemFP289
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP289.png)
## Feature : PubchemFP290
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP290.png)
## Feature : PubchemFP291
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP291.png)
## Feature : PubchemFP292
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP292.png)
## Feature : PubchemFP293
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3008849557522124
- **Std** :0.46068574182199146
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP293.png)
## Feature : PubchemFP294
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP294.png)
## Feature : PubchemFP295
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP295.png)
## Feature : PubchemFP296
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP296.png)
## Feature : PubchemFP297
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP297.png)
## Feature : PubchemFP298
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP298.png)
## Feature : PubchemFP299
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7345132743362832
- **Std** :0.44355904573614724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP299.png)
## Feature : PubchemFP300
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2920353982300885
- **Std** :0.45672389145028497
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP300.png)
## Feature : PubchemFP301
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP301.png)
## Feature : PubchemFP302
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP302.png)
## Feature : PubchemFP303
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP303.png)
## Feature : PubchemFP304
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP304.png)
## Feature : PubchemFP305
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP305.png)
## Feature : PubchemFP306
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP306.png)
## Feature : PubchemFP307
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP307.png)
## Feature : PubchemFP308
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.30973451327433627
- **Std** :0.46444371521398564
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP308.png)
## Feature : PubchemFP309
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP309.png)
## Feature : PubchemFP310
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP310.png)
## Feature : PubchemFP311
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP311.png)
## Feature : PubchemFP312
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP312.png)
## Feature : PubchemFP313
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP313.png)
## Feature : PubchemFP314
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP314.png)
## Feature : PubchemFP315
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP315.png)
## Feature : PubchemFP316
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP316.png)
## Feature : PubchemFP317
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP317.png)
## Feature : PubchemFP318
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP318.png)
## Feature : PubchemFP319
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP319.png)
## Feature : PubchemFP320
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP320.png)
## Feature : PubchemFP321
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP321.png)
## Feature : PubchemFP322
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP322.png)
## Feature : PubchemFP323
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP323.png)
## Feature : PubchemFP324
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP324.png)
## Feature : PubchemFP325
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP325.png)
## Feature : PubchemFP326
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP326.png)
## Feature : PubchemFP327
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP327.png)
## Feature : PubchemFP328
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP328.png)
## Feature : PubchemFP329
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP329.png)
## Feature : PubchemFP330
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP330.png)
## Feature : PubchemFP331
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP331.png)
## Feature : PubchemFP332
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP332.png)
## Feature : PubchemFP333
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.911504424778761
- **Std** :0.28527937822590804
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP333.png)
## Feature : PubchemFP334
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP334.png)
## Feature : PubchemFP335
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.350185069365577
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP335.png)
## Feature : PubchemFP336
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP336.png)
## Feature : PubchemFP337
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP337.png)
## Feature : PubchemFP338
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221803
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP338.png)
## Feature : PubchemFP339
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487182
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP339.png)
## Feature : PubchemFP340
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7610619469026548
- **Std** :0.4283343212622304
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP340.png)
## Feature : PubchemFP341
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6106194690265486
- **Std** :0.48978180853796094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP341.png)
## Feature : PubchemFP342
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP342.png)
## Feature : PubchemFP343
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP343.png)
## Feature : PubchemFP344
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP344.png)
## Feature : PubchemFP345
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6194690265486725
- **Std** :0.4876800779271999
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP345.png)
## Feature : PubchemFP346
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.26548672566371684
- **Std** :0.44355904573614724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP346.png)
## Feature : PubchemFP347
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543103
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP347.png)
## Feature : PubchemFP348
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP348.png)
## Feature : PubchemFP349
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093984
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP349.png)
## Feature : PubchemFP350
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP350.png)
## Feature : PubchemFP351
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9026548672566371
- **Std** :0.29774754916562113
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP351.png)
## Feature : PubchemFP352
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8672566371681416
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP352.png)
## Feature : PubchemFP353
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.43365089339828705
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP353.png)
## Feature : PubchemFP354
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP354.png)
## Feature : PubchemFP355
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP355.png)
## Feature : PubchemFP356
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8849557522123894
- **Std** :0.3204966121221803
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP356.png)
## Feature : PubchemFP357
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.34513274336283184
- **Std** :0.4775291970210675
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP357.png)
## Feature : PubchemFP358
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.45132743362831856
- **Std** :0.4998419472064193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP358.png)
## Feature : PubchemFP359
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487162
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP359.png)
## Feature : PubchemFP360
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP360.png)
## Feature : PubchemFP361
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP361.png)
## Feature : PubchemFP362
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590804
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP362.png)
## Feature : PubchemFP363
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720252
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP363.png)
## Feature : PubchemFP364
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720235
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP364.png)
## Feature : PubchemFP365
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6637168141592921
- **Std** :0.4745414980541471
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP365.png)
## Feature : PubchemFP366
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3893805309734513
- **Std** :0.4897818085379609
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP366.png)
## Feature : PubchemFP367
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP367.png)
## Feature : PubchemFP368
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221803
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP368.png)
## Feature : PubchemFP369
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP369.png)
## Feature : PubchemFP370
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP370.png)
## Feature : PubchemFP371
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP371.png)
## Feature : PubchemFP372
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.3675782638096066
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP372.png)
## Feature : PubchemFP373
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.375657872159359
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP373.png)
## Feature : PubchemFP374
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6371681415929203
- **Std** :0.4829586440194673
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP374.png)
## Feature : PubchemFP375
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.35398230088495575
- **Std** :0.4803338493452254
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP375.png)
## Feature : PubchemFP376
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7787610619469026
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP376.png)
## Feature : PubchemFP377
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6902654867256637
- **Std** :0.46444371521398553
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP377.png)
## Feature : PubchemFP378
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656213
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP378.png)
## Feature : PubchemFP379
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP379.png)
## Feature : PubchemFP380
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.30973451327433627
- **Std** :0.46444371521398564
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP380.png)
## Feature : PubchemFP381
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5575221238938053
- **Std** :0.4988925789283044
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP381.png)
## Feature : PubchemFP382
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.49557522123893805
- **Std** :0.5022075162525259
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP382.png)
## Feature : PubchemFP383
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935886
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP383.png)
## Feature : PubchemFP384
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP384.png)
## Feature : PubchemFP385
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.43872300612823406
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP385.png)
## Feature : PubchemFP386
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.30973451327433627
- **Std** :0.46444371521398564
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP386.png)
## Feature : PubchemFP387
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.45132743362831856
- **Std** :0.4998419472064193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP387.png)
## Feature : PubchemFP388
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.2065611519179461
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP388.png)
## Feature : PubchemFP389
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960634
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP389.png)
## Feature : PubchemFP390
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.39071072068301
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP390.png)
## Feature : PubchemFP391
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :0.47136727180667504
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP391.png)
## Feature : PubchemFP392
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5221238938053098
- **Std** :0.5017352946941697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP392.png)
## Feature : PubchemFP393
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7256637168141593
- **Std** :0.44816665448205023
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP393.png)
## Feature : PubchemFP394
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.4169299631647697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP394.png)
## Feature : PubchemFP395
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP395.png)
## Feature : PubchemFP396
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3893805309734513
- **Std** :0.48978180853796094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP396.png)
## Feature : PubchemFP397
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP397.png)
## Feature : PubchemFP398
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.4169299631647697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP398.png)
## Feature : PubchemFP399
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656211
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP399.png)
## Feature : PubchemFP400
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.2852793782259081
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP400.png)
## Feature : PubchemFP401
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP401.png)
## Feature : PubchemFP402
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP402.png)
## Feature : PubchemFP403
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP403.png)
## Feature : PubchemFP404
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543109
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP404.png)
## Feature : PubchemFP405
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.46017699115044247
- **Std** :0.500631712191287
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP405.png)
## Feature : PubchemFP406
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3008849557522124
- **Std** :0.46068574182199146
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP406.png)
## Feature : PubchemFP407
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP407.png)
## Feature : PubchemFP408
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP408.png)
## Feature : PubchemFP409
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP409.png)
## Feature : PubchemFP410
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP410.png)
## Feature : PubchemFP411
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP411.png)
## Feature : PubchemFP412
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646191
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP412.png)
## Feature : PubchemFP413
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP413.png)
## Feature : PubchemFP414
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562096
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP414.png)
## Feature : PubchemFP415
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP415.png)
## Feature : PubchemFP416
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP416.png)
## Feature : PubchemFP417
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP417.png)
## Feature : PubchemFP418
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6106194690265486
- **Std** :0.489781808537961
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP418.png)
## Feature : PubchemFP419
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.2719503933346899
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP419.png)
## Feature : PubchemFP420
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7787610619469026
- **Std** :0.4169299631647696
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP420.png)
## Feature : PubchemFP421
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960656
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP421.png)
## Feature : PubchemFP422
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964092
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP422.png)
## Feature : PubchemFP423
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935886
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP423.png)
## Feature : PubchemFP424
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP424.png)
## Feature : PubchemFP425
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP425.png)
## Feature : PubchemFP426
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP426.png)
## Feature : PubchemFP427
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP427.png)
## Feature : PubchemFP428
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835976
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP428.png)
## Feature : PubchemFP429
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.2719503933346899
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP429.png)
## Feature : PubchemFP430
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9026548672566371
- **Std** :0.29774754916562113
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP430.png)
## Feature : PubchemFP431
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3893805309734513
- **Std** :0.4897818085379609
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP431.png)
## Feature : PubchemFP432
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.30973451327433627
- **Std** :0.46444371521398564
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP432.png)
## Feature : PubchemFP433
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP433.png)
## Feature : PubchemFP434
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9911504424778761
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP434.png)
## Feature : PubchemFP435
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.26548672566371684
- **Std** :0.44355904573614724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP435.png)
## Feature : PubchemFP436
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP436.png)
## Feature : PubchemFP437
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7256637168141593
- **Std** :0.4481666544820499
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP437.png)
## Feature : PubchemFP438
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP438.png)
## Feature : PubchemFP439
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4424778761061947
- **Std** :0.4988925789283046
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP439.png)
## Feature : PubchemFP440
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2831858407079646
- **Std** :0.452552809638842
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP440.png)
## Feature : PubchemFP441
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :1.0
- **Std** :0.0
- **Min** :1.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP441.png)
## Feature : PubchemFP442
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6017699115044248
- **Std** :0.4917138941797926
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP442.png)
## Feature : PubchemFP443
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7610619469026548
- **Std** :0.4283343212622306
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP443.png)
## Feature : PubchemFP444
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP444.png)
## Feature : PubchemFP445
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.18584070796460178
- **Std** :0.39071072068301
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP445.png)
## Feature : PubchemFP446
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9911504424778761
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP446.png)
## Feature : PubchemFP447
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2743362831858407
- **Std** :0.44816665448205006
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP447.png)
## Feature : PubchemFP448
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP448.png)
## Feature : PubchemFP449
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8053097345132744
- **Std** :0.39772583610819723
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP449.png)
## Feature : PubchemFP450
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.433650893398287
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP450.png)
## Feature : PubchemFP451
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4690265486725664
- **Std** :0.5012626282730047
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP451.png)
## Feature : PubchemFP452
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2920353982300885
- **Std** :0.4567238914502851
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP452.png)
## Feature : PubchemFP453
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.46017699115044247
- **Std** :0.500631712191287
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP453.png)
## Feature : PubchemFP454
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935886
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP454.png)
## Feature : PubchemFP455
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP455.png)
## Feature : PubchemFP456
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP456.png)
## Feature : PubchemFP457
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562096
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP457.png)
## Feature : PubchemFP458
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP458.png)
## Feature : PubchemFP459
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562096
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP459.png)
## Feature : PubchemFP460
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP460.png)
## Feature : PubchemFP461
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.1856072822146943
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP461.png)
## Feature : PubchemFP462
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.35018506936557714
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP462.png)
## Feature : PubchemFP463
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP463.png)
## Feature : PubchemFP464
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8407079646017699
- **Std** :0.3675782638096065
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP464.png)
## Feature : PubchemFP465
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562096
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP465.png)
## Feature : PubchemFP466
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP466.png)
## Feature : PubchemFP467
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.43872300612823395
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP467.png)
## Feature : PubchemFP468
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP468.png)
## Feature : PubchemFP469
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP469.png)
## Feature : PubchemFP470
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :0.1614762803872023
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP470.png)
## Feature : PubchemFP471
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647725
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP471.png)
## Feature : PubchemFP472
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP472.png)
## Feature : PubchemFP473
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964087
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP473.png)
## Feature : PubchemFP474
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.2065611519179459
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP474.png)
## Feature : PubchemFP475
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469007
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP475.png)
## Feature : PubchemFP476
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.336283185840708
- **Std** :0.47454149805414636
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP476.png)
## Feature : PubchemFP477
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP477.png)
## Feature : PubchemFP478
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP478.png)
## Feature : PubchemFP479
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP479.png)
## Feature : PubchemFP480
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469007
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP480.png)
## Feature : PubchemFP481
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP481.png)
## Feature : PubchemFP482
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP482.png)
## Feature : PubchemFP483
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP483.png)
## Feature : PubchemFP484
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP484.png)
## Feature : PubchemFP485
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1504424778761062
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP485.png)
## Feature : PubchemFP486
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP486.png)
## Feature : PubchemFP487
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.42833432126223064
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP487.png)
## Feature : PubchemFP488
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.2852793782259081
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP488.png)
## Feature : PubchemFP489
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221803
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP489.png)
## Feature : PubchemFP490
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP490.png)
## Feature : PubchemFP491
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.375657872159359
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP491.png)
## Feature : PubchemFP492
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543078
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP492.png)
## Feature : PubchemFP493
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5398230088495575
- **Std** :0.500631712191287
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP493.png)
## Feature : PubchemFP494
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP494.png)
## Feature : PubchemFP495
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6460176991150443
- **Std** :0.4803338493452255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP495.png)
## Feature : PubchemFP496
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP496.png)
## Feature : PubchemFP497
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.1856072822146943
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP497.png)
## Feature : PubchemFP498
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5752212389380531
- **Std** :0.4965112165060408
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP498.png)
## Feature : PubchemFP499
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935886
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP499.png)
## Feature : PubchemFP500
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562096
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP500.png)
## Feature : PubchemFP501
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590804
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP501.png)
## Feature : PubchemFP502
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7168141592920354
- **Std** :0.4525528096388417
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP502.png)
## Feature : PubchemFP503
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590804
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP503.png)
## Feature : PubchemFP504
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960634
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP504.png)
## Feature : PubchemFP505
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP505.png)
## Feature : PubchemFP506
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3805309734513274
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP506.png)
## Feature : PubchemFP507
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.375657872159359
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP507.png)
## Feature : PubchemFP508
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.13274336283185842
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP508.png)
## Feature : PubchemFP509
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP509.png)
## Feature : PubchemFP510
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP510.png)
## Feature : PubchemFP511
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562113
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP511.png)
## Feature : PubchemFP512
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP512.png)
## Feature : PubchemFP513
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.2421328531696409
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP513.png)
## Feature : PubchemFP514
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.37565787215935886
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP514.png)
## Feature : PubchemFP515
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.4169299631647697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP515.png)
## Feature : PubchemFP516
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP516.png)
## Feature : PubchemFP517
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995906
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP517.png)
## Feature : PubchemFP518
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.3675782638096065
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP518.png)
## Feature : PubchemFP519
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.4227640670448753
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP519.png)
## Feature : PubchemFP520
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :0.1614762803872023
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP520.png)
## Feature : PubchemFP521
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.42833432126223064
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP521.png)
## Feature : PubchemFP522
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487182
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP522.png)
## Feature : PubchemFP523
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.45132743362831856
- **Std** :0.4998419472064193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP523.png)
## Feature : PubchemFP524
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP524.png)
## Feature : PubchemFP525
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP525.png)
## Feature : PubchemFP526
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP526.png)
## Feature : PubchemFP527
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656211
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP527.png)
## Feature : PubchemFP528
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2743362831858407
- **Std** :0.4481666544820501
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP528.png)
## Feature : PubchemFP529
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP529.png)
## Feature : PubchemFP530
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.45132743362831856
- **Std** :0.4998419472064193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP530.png)
## Feature : PubchemFP531
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960634
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP531.png)
## Feature : PubchemFP532
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960634
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP532.png)
## Feature : PubchemFP533
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647725
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP533.png)
## Feature : PubchemFP534
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.1324427696454309
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP534.png)
## Feature : PubchemFP535
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7079646017699115
- **Std** :0.4567238914502851
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP535.png)
## Feature : PubchemFP536
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.35018506936557714
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP536.png)
## Feature : PubchemFP537
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.35018506936557714
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP537.png)
## Feature : PubchemFP538
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5929203539823009
- **Std** :0.49347832745856457
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP538.png)
## Feature : PubchemFP539
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4424778761061947
- **Std** :0.49889257892830463
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP539.png)
## Feature : PubchemFP540
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5663716814159292
- **Std** :0.49778269417434456
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP540.png)
## Feature : PubchemFP541
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5486725663716814
- **Std** :0.49984194720641906
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP541.png)
## Feature : PubchemFP542
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4778761061946903
- **Std** :0.5017352946941697
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP542.png)
## Feature : PubchemFP543
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647725
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP543.png)
## Feature : PubchemFP544
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590815
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP544.png)
## Feature : PubchemFP545
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7787610619469026
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP545.png)
## Feature : PubchemFP546
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7079646017699115
- **Std** :0.4567238914502849
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP546.png)
## Feature : PubchemFP547
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.20353982300884957
- **Std** :0.40442401801376365
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP547.png)
## Feature : PubchemFP548
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6283185840707964
- **Std** :0.4854064987477493
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP548.png)
## Feature : PubchemFP549
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.39071072068301005
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP549.png)
## Feature : PubchemFP550
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656212
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP550.png)
## Feature : PubchemFP551
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP551.png)
## Feature : PubchemFP552
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :0.1614762803872023
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP552.png)
## Feature : PubchemFP553
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6460176991150443
- **Std** :0.4803338493452255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP553.png)
## Feature : PubchemFP554
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP554.png)
## Feature : PubchemFP555
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6017699115044248
- **Std** :0.4917138941797926
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP555.png)
## Feature : PubchemFP556
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP556.png)
## Feature : PubchemFP557
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543106
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP557.png)
## Feature : PubchemFP558
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960645
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP558.png)
## Feature : PubchemFP559
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720235
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP559.png)
## Feature : PubchemFP560
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :0.47136727180667504
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP560.png)
## Feature : PubchemFP561
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.132442769645431
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP561.png)
## Feature : PubchemFP562
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP562.png)
## Feature : PubchemFP563
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP563.png)
## Feature : PubchemFP564
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :0.1614762803872023
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP564.png)
## Feature : PubchemFP565
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.336283185840708
- **Std** :0.47454149805414636
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP565.png)
## Feature : PubchemFP566
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP566.png)
## Feature : PubchemFP567
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.18584070796460178
- **Std** :0.39071072068300994
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP567.png)
## Feature : PubchemFP568
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP568.png)
## Feature : PubchemFP569
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2831858407079646
- **Std** :0.4525528096388418
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP569.png)
## Feature : PubchemFP570
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9823008849557522
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP570.png)
## Feature : PubchemFP571
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP571.png)
## Feature : PubchemFP572
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960634
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP572.png)
## Feature : PubchemFP573
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6460176991150443
- **Std** :0.4803338493452254
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP573.png)
## Feature : PubchemFP574
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6194690265486725
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP574.png)
## Feature : PubchemFP575
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.13274336283185842
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP575.png)
## Feature : PubchemFP576
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.504424778761062
- **Std** :0.5022075162525258
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP576.png)
## Feature : PubchemFP577
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.4283343212622307
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP577.png)
## Feature : PubchemFP578
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP578.png)
## Feature : PubchemFP579
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6814159292035398
- **Std** :0.46800272293285433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP579.png)
## Feature : PubchemFP580
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.42276406704487524
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP580.png)
## Feature : PubchemFP581
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995908
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP581.png)
## Feature : PubchemFP582
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9734513274336283
- **Std** :0.16147628038720235
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP582.png)
## Feature : PubchemFP583
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP583.png)
## Feature : PubchemFP584
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP584.png)
## Feature : PubchemFP585
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6460176991150443
- **Std** :0.4803338493452255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP585.png)
## Feature : PubchemFP586
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995904
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP586.png)
## Feature : PubchemFP587
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.1614762803872025
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP587.png)
## Feature : PubchemFP588
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720246
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP588.png)
## Feature : PubchemFP589
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :0.47136727180667504
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP589.png)
## Feature : PubchemFP590
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.4227640670448753
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP590.png)
## Feature : PubchemFP591
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656212
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP591.png)
## Feature : PubchemFP592
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8938053097345132
- **Std** :0.30945897080939855
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP592.png)
## Feature : PubchemFP593
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.4283343212622306
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP593.png)
## Feature : PubchemFP594
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4336283185840708
- **Std** :0.49778269417434445
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP594.png)
## Feature : PubchemFP595
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP595.png)
## Feature : PubchemFP596
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP596.png)
## Feature : PubchemFP597
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5929203539823009
- **Std** :0.4934783274585646
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP597.png)
## Feature : PubchemFP598
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590804
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP598.png)
## Feature : PubchemFP599
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP599.png)
## Feature : PubchemFP600
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7787610619469026
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP600.png)
## Feature : PubchemFP601
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1504424778761062
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP601.png)
## Feature : PubchemFP602
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.13274336283185842
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP602.png)
## Feature : PubchemFP603
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP603.png)
## Feature : PubchemFP604
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.415929203539823
- **Std** :0.4950769008954596
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP604.png)
## Feature : PubchemFP605
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.30945897080939866
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP605.png)
## Feature : PubchemFP606
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5309734513274337
- **Std** :0.5012626282730048
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP606.png)
## Feature : PubchemFP607
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.831858407079646
- **Std** :0.3756578721593589
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP607.png)
## Feature : PubchemFP608
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP608.png)
## Feature : PubchemFP609
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.132442769645431
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP609.png)
## Feature : PubchemFP610
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP610.png)
## Feature : PubchemFP611
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2743362831858407
- **Std** :0.4481666544820501
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP611.png)
## Feature : PubchemFP612
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.22522639263487182
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP612.png)
## Feature : PubchemFP613
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7610619469026548
- **Std** :0.4283343212622305
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP613.png)
## Feature : PubchemFP614
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.34513274336283184
- **Std** :0.4775291970210676
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP614.png)
## Feature : PubchemFP615
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995906
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP615.png)
## Feature : PubchemFP616
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964092
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP616.png)
## Feature : PubchemFP617
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.438723006128234
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP617.png)
## Feature : PubchemFP618
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP618.png)
## Feature : PubchemFP619
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6283185840707964
- **Std** :0.4854064987477493
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP619.png)
## Feature : PubchemFP620
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.42276406704487535
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP620.png)
## Feature : PubchemFP621
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.35018506936557703
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP621.png)
## Feature : PubchemFP622
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.2852793782259081
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP622.png)
## Feature : PubchemFP623
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5398230088495575
- **Std** :0.500631712191287
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP623.png)
## Feature : PubchemFP624
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960645
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP624.png)
## Feature : PubchemFP625
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP625.png)
## Feature : PubchemFP626
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :0.47136727180667504
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP626.png)
## Feature : PubchemFP627
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP627.png)
## Feature : PubchemFP628
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6902654867256637
- **Std** :0.46444371521398553
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP628.png)
## Feature : PubchemFP629
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543109
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP629.png)
## Feature : PubchemFP630
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.25762701996477244
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP630.png)
## Feature : PubchemFP631
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP631.png)
## Feature : PubchemFP632
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.42276406704487535
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP632.png)
## Feature : PubchemFP633
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8141592920353983
- **Std** :0.39071072068301005
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP633.png)
## Feature : PubchemFP634
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP634.png)
## Feature : PubchemFP635
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093987
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP635.png)
## Feature : PubchemFP636
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.438723006128234
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP636.png)
## Feature : PubchemFP637
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6371681415929203
- **Std** :0.4829586440194674
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP637.png)
## Feature : PubchemFP638
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.43365089339828705
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP638.png)
## Feature : PubchemFP639
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.3501850693655772
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP639.png)
## Feature : PubchemFP640
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP640.png)
## Feature : PubchemFP641
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6106194690265486
- **Std** :0.489781808537961
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP641.png)
## Feature : PubchemFP642
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995908
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP642.png)
## Feature : PubchemFP643
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5663716814159292
- **Std** :0.49778269417434445
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP643.png)
## Feature : PubchemFP644
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.43872300612823395
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP644.png)
## Feature : PubchemFP645
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.34513274336283184
- **Std** :0.4775291970210676
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP645.png)
## Feature : PubchemFP646
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.20353982300884957
- **Std** :0.40442401801376365
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP646.png)
## Feature : PubchemFP647
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590826
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP647.png)
## Feature : PubchemFP648
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720246
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP648.png)
## Feature : PubchemFP649
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543097
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP649.png)
## Feature : PubchemFP650
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.20656115191794622
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP650.png)
## Feature : PubchemFP651
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5309734513274337
- **Std** :0.5012626282730048
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP651.png)
## Feature : PubchemFP652
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093986
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP652.png)
## Feature : PubchemFP653
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093988
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP653.png)
## Feature : PubchemFP654
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.3274336283185841
- **Std** :0.47136727180667504
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP654.png)
## Feature : PubchemFP655
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6106194690265486
- **Std** :0.489781808537961
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP655.png)
## Feature : PubchemFP656
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6017699115044248
- **Std** :0.49171389417979255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP656.png)
## Feature : PubchemFP657
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6460176991150443
- **Std** :0.4803338493452255
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP657.png)
## Feature : PubchemFP658
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656211
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP658.png)
## Feature : PubchemFP659
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.3675782638096064
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP659.png)
## Feature : PubchemFP660
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP660.png)
## Feature : PubchemFP661
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720263
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP661.png)
## Feature : PubchemFP662
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.36757826380960656
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP662.png)
## Feature : PubchemFP663
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647725
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP663.png)
## Feature : PubchemFP664
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP664.png)
## Feature : PubchemFP665
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.7787610619469026
- **Std** :0.41692996316476966
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP665.png)
## Feature : PubchemFP666
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.4336283185840708
- **Std** :0.49778269417434445
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP666.png)
## Feature : PubchemFP667
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.4336508933982868
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP667.png)
## Feature : PubchemFP668
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP668.png)
## Feature : PubchemFP669
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP669.png)
## Feature : PubchemFP670
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP670.png)
## Feature : PubchemFP671
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6194690265486725
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP671.png)
## Feature : PubchemFP672
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5575221238938053
- **Std** :0.4988925789283045
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP672.png)
## Feature : PubchemFP673
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.42833432126223064
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP673.png)
## Feature : PubchemFP674
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.4336508933982869
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP674.png)
## Feature : PubchemFP675
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP675.png)
## Feature : PubchemFP676
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.1614762803872026
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP676.png)
## Feature : PubchemFP677
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP677.png)
## Feature : PubchemFP678
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP678.png)
## Feature : PubchemFP679
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9646017699115044
- **Std** :0.18560728221469433
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP679.png)
## Feature : PubchemFP680
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6194690265486725
- **Std** :0.4876800779272
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP680.png)
## Feature : PubchemFP681
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP681.png)
## Feature : PubchemFP682
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.42833432126223064
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP682.png)
## Feature : PubchemFP683
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8495575221238938
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP683.png)
## Feature : PubchemFP684
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.6283185840707964
- **Std** :0.48540649874774944
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP684.png)
## Feature : PubchemFP685
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.21238938053097345
- **Std** :0.4108207689150269
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP685.png)
## Feature : PubchemFP686
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1504424778761062
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP686.png)
## Feature : PubchemFP687
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.2852793782259083
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP687.png)
## Feature : PubchemFP688
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8672566371681416
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP688.png)
## Feature : PubchemFP689
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5929203539823009
- **Std** :0.49347832745856457
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP689.png)
## Feature : PubchemFP690
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.168141592920354
- **Std** :0.375657872159359
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP690.png)
## Feature : PubchemFP691
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1415929203539823
- **Std** :0.3501850693655769
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP691.png)
## Feature : PubchemFP692
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5752212389380531
- **Std** :0.49651121650604085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP692.png)
## Feature : PubchemFP693
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1592920353982301
- **Std** :0.3675782638096065
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP693.png)
## Feature : PubchemFP694
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333469
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP694.png)
## Feature : PubchemFP695
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221802
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP695.png)
## Feature : PubchemFP696
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5575221238938053
- **Std** :0.49889257892830463
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP696.png)
## Feature : PubchemFP697
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.45132743362831856
- **Std** :0.49984194720641906
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP697.png)
## Feature : PubchemFP698
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.504424778761062
- **Std** :0.5022075162525258
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP698.png)
## Feature : PubchemFP699
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.336283185840708
- **Std** :0.4745414980541464
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP699.png)
## Feature : PubchemFP700
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.13274336283185842
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP700.png)
## Feature : PubchemFP701
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.1504424778761062
- **Std** :0.35909705557464094
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP701.png)
## Feature : PubchemFP702
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.30945897080939855
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP702.png)
## Feature : PubchemFP703
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590804
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP703.png)
## Feature : PubchemFP704
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.5752212389380531
- **Std** :0.49651121650604085
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP704.png)
## Feature : PubchemFP705
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.27195039333468973
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP705.png)
## Feature : PubchemFP706
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543106
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP706.png)
## Feature : PubchemFP707
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.061946902654867256
- **Std** :0.24213285316964078
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP707.png)
## Feature : PubchemFP708
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.9026548672566371
- **Std** :0.2977475491656213
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP708.png)
## Feature : PubchemFP709
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8672566371681416
- **Std** :0.34080851291872993
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP709.png)
## Feature : PubchemFP710
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.8761061946902655
- **Std** :0.33092789899959085
- **Min** :0.0
- **25%th Percentile** : 1.0
- **50%th Percentile** : 1.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP710.png)
## Feature : PubchemFP711
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835983
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP711.png)
## Feature : PubchemFP712
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.37168141592920356
- **Std** :0.4854064987477493
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP712.png)
## Feature : PubchemFP713
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995906
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP713.png)
## Feature : PubchemFP714
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.19469026548672566
- **Std** :0.397725836108197
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP714.png)
## Feature : PubchemFP715
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543097
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP715.png)
## Feature : PubchemFP716
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562124
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP716.png)
## Feature : PubchemFP717
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.1614762803872024
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP717.png)
## Feature : PubchemFP718
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP718.png)
## Feature : PubchemFP719
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP719.png)
## Feature : PubchemFP720
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP720.png)
## Feature : PubchemFP721
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093988
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP721.png)
## Feature : PubchemFP722
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835977
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP722.png)
## Feature : PubchemFP723
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.1324427696454309
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP723.png)
## Feature : PubchemFP724
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP724.png)
## Feature : PubchemFP725
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.206561151917946
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP725.png)
## Feature : PubchemFP726
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP726.png)
## Feature : PubchemFP727
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP727.png)
## Feature : PubchemFP728
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.2719503933346899
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP728.png)
## Feature : PubchemFP729
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP729.png)
## Feature : PubchemFP730
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720252
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP730.png)
## Feature : PubchemFP731
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP731.png)
## Feature : PubchemFP732
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP732.png)
## Feature : PubchemFP733
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP733.png)
## Feature : PubchemFP734
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.3309278989995904
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP734.png)
## Feature : PubchemFP735
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.22123893805309736
- **Std** :0.4169299631647696
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP735.png)
## Feature : PubchemFP736
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP736.png)
## Feature : PubchemFP737
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23893805309734514
- **Std** :0.4283343212622306
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP737.png)
## Feature : PubchemFP738
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543095
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP738.png)
## Feature : PubchemFP739
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720244
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP739.png)
## Feature : PubchemFP740
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.10619469026548672
- **Std** :0.3094589708093988
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP740.png)
## Feature : PubchemFP741
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP741.png)
## Feature : PubchemFP742
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.20656115191794605
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP742.png)
## Feature : PubchemFP743
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP743.png)
## Feature : PubchemFP744
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP744.png)
## Feature : PubchemFP745
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP745.png)
## Feature : PubchemFP746
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543109
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP746.png)
## Feature : PubchemFP747
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP747.png)
## Feature : PubchemFP748
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP748.png)
## Feature : PubchemFP749
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP749.png)
## Feature : PubchemFP750
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.2252263926348718
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP750.png)
## Feature : PubchemFP751
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP751.png)
## Feature : PubchemFP752
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP752.png)
## Feature : PubchemFP753
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP753.png)
## Feature : PubchemFP754
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP754.png)
## Feature : PubchemFP755
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.42276406704487524
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP755.png)
## Feature : PubchemFP756
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.25663716814159293
- **Std** :0.43872300612823395
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP756.png)
## Feature : PubchemFP757
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835988
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP757.png)
## Feature : PubchemFP758
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.30973451327433627
- **Std** :0.4644437152139857
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP758.png)
## Feature : PubchemFP759
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543097
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP759.png)
## Feature : PubchemFP760
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP760.png)
## Feature : PubchemFP761
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.08849557522123894
- **Std** :0.28527937822590815
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP761.png)
## Feature : PubchemFP762
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP762.png)
## Feature : PubchemFP763
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.2065611519179461
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP763.png)
## Feature : PubchemFP764
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP764.png)
## Feature : PubchemFP765
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP765.png)
## Feature : PubchemFP766
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP766.png)
## Feature : PubchemFP767
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.1856072822146943
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP767.png)
## Feature : PubchemFP768
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP768.png)
## Feature : PubchemFP769
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP769.png)
## Feature : PubchemFP770
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP770.png)
## Feature : PubchemFP771
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP771.png)
## Feature : PubchemFP772
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP772.png)
## Feature : PubchemFP773
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP773.png)
## Feature : PubchemFP774
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP774.png)
## Feature : PubchemFP775
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP775.png)
## Feature : PubchemFP776
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.21238938053097345
- **Std** :0.41082076891502695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP776.png)
## Feature : PubchemFP777
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.21238938053097345
- **Std** :0.41082076891502683
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP777.png)
## Feature : PubchemFP778
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543097
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP778.png)
## Feature : PubchemFP779
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.29774754916562124
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP779.png)
## Feature : PubchemFP780
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.1614762803872024
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP780.png)
## Feature : PubchemFP781
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP781.png)
## Feature : PubchemFP782
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543086
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP782.png)
## Feature : PubchemFP783
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP783.png)
## Feature : PubchemFP784
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.11504424778761062
- **Std** :0.3204966121221803
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP784.png)
## Feature : PubchemFP785
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835977
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP785.png)
## Feature : PubchemFP786
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.1324427696454309
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP786.png)
## Feature : PubchemFP787
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP787.png)
## Feature : PubchemFP788
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.206561151917946
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP788.png)
## Feature : PubchemFP789
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP789.png)
## Feature : PubchemFP790
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP790.png)
## Feature : PubchemFP791
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07964601769911504
- **Std** :0.2719503933346899
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP791.png)
## Feature : PubchemFP792
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP792.png)
## Feature : PubchemFP793
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720252
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP793.png)
## Feature : PubchemFP794
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP794.png)
## Feature : PubchemFP795
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP795.png)
## Feature : PubchemFP796
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP796.png)
## Feature : PubchemFP797
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.21238938053097345
- **Std** :0.41082076891502695
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP797.png)
## Feature : PubchemFP798
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.4336508933982869
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP798.png)
## Feature : PubchemFP799
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP799.png)
## Feature : PubchemFP800
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.24778761061946902
- **Std** :0.433650893398287
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP800.png)
## Feature : PubchemFP801
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543095
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP801.png)
## Feature : PubchemFP802
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720244
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP802.png)
## Feature : PubchemFP803
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.12389380530973451
- **Std** :0.33092789899959074
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP803.png)
## Feature : PubchemFP804
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP804.png)
## Feature : PubchemFP805
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.20656115191794605
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP805.png)
## Feature : PubchemFP806
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP806.png)
## Feature : PubchemFP807
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP807.png)
## Feature : PubchemFP808
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP808.png)
## Feature : PubchemFP809
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543109
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP809.png)
## Feature : PubchemFP810
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP810.png)
## Feature : PubchemFP811
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP811.png)
## Feature : PubchemFP812
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.07079646017699115
- **Std** :0.2576270199647724
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP812.png)
## Feature : PubchemFP813
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.05309734513274336
- **Std** :0.2252263926348718
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP813.png)
## Feature : PubchemFP814
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543084
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP814.png)
## Feature : PubchemFP815
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP815.png)
## Feature : PubchemFP816
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP816.png)
## Feature : PubchemFP817
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP817.png)
## Feature : PubchemFP818
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.23008849557522124
- **Std** :0.42276406704487524
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP818.png)
## Feature : PubchemFP819
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.2831858407079646
- **Std** :0.4525528096388417
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP819.png)
## Feature : PubchemFP820
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835988
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP820.png)
## Feature : PubchemFP821
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.30973451327433627
- **Std** :0.4644437152139857
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 1.0
- **Max** :1.0

![](PubchemFP821.png)
## Feature : PubchemFP822
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543097
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP822.png)
## Feature : PubchemFP823
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP823.png)
## Feature : PubchemFP824
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.09734513274336283
- **Std** :0.2977475491656212
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP824.png)
## Feature : PubchemFP825
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP825.png)
## Feature : PubchemFP826
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.04424778761061947
- **Std** :0.2065611519179461
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP826.png)
## Feature : PubchemFP827
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP827.png)
## Feature : PubchemFP828
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP828.png)
## Feature : PubchemFP829
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP829.png)
## Feature : PubchemFP830
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.035398230088495575
- **Std** :0.1856072822146943
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP830.png)
## Feature : PubchemFP831
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP831.png)
## Feature : PubchemFP832
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP832.png)
## Feature : PubchemFP833
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.17699115044247787
- **Std** :0.3833612734646193
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP833.png)
## Feature : PubchemFP834
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.0940720868383598
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP834.png)
## Feature : PubchemFP835
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP835.png)
## Feature : PubchemFP836
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP836.png)
## Feature : PubchemFP837
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP837.png)
## Feature : PubchemFP838
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP838.png)
## Feature : PubchemFP839
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.017699115044247787
- **Std** :0.13244276964543109
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP839.png)
## Feature : PubchemFP840
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP840.png)
## Feature : PubchemFP841
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP841.png)
## Feature : PubchemFP842
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP842.png)
## Feature : PubchemFP843
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP843.png)
## Feature : PubchemFP844
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP844.png)
## Feature : PubchemFP845
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP845.png)
## Feature : PubchemFP846
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP846.png)
## Feature : PubchemFP847
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP847.png)
## Feature : PubchemFP848
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP848.png)
## Feature : PubchemFP849
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP849.png)
## Feature : PubchemFP850
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP850.png)
## Feature : PubchemFP851
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP851.png)
## Feature : PubchemFP852
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP852.png)
## Feature : PubchemFP853
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP853.png)
## Feature : PubchemFP854
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP854.png)
## Feature : PubchemFP855
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP855.png)
## Feature : PubchemFP856
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP856.png)
## Feature : PubchemFP857
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP857.png)
## Feature : PubchemFP858
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP858.png)
## Feature : PubchemFP859
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP859.png)
## Feature : PubchemFP860
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.02654867256637168
- **Std** :0.16147628038720266
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP860.png)
## Feature : PubchemFP861
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP861.png)
## Feature : PubchemFP862
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP862.png)
## Feature : PubchemFP863
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP863.png)
## Feature : PubchemFP864
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP864.png)
## Feature : PubchemFP865
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP865.png)
## Feature : PubchemFP866
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP866.png)
## Feature : PubchemFP867
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP867.png)
## Feature : PubchemFP868
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP868.png)
## Feature : PubchemFP869
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP869.png)
## Feature : PubchemFP870
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP870.png)
## Feature : PubchemFP871
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP871.png)
## Feature : PubchemFP872
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP872.png)
## Feature : PubchemFP873
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 2
- **Count** :113.0
- **Mean** :0.008849557522123894
- **Std** :0.09407208683835974
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :1.0

![](PubchemFP873.png)
## Feature : PubchemFP874
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP874.png)
## Feature : PubchemFP875
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP875.png)
## Feature : PubchemFP876
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP876.png)
## Feature : PubchemFP877
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP877.png)
## Feature : PubchemFP878
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP878.png)
## Feature : PubchemFP879
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP879.png)
## Feature : PubchemFP880
- **Feature type** : discrete
- **Missing** : 0.0%
- **Unique** : 1
- **Count** :113.0
- **Mean** :0.0
- **Std** :0.0
- **Min** :0.0
- **25%th Percentile** : 0.0
- **50%th Percentile** : 0.0
- **75%th Percentile** : 0.0
- **Max** :0.0

![](PubchemFP880.png)


[<< Go back](../README.md)
